#!/usr/bin/env bash

set -euo pipefail

zola build

cd public; tar -cvz * > ../site.tar.gz

cd ..

token=$(cat /home/jaxter/.srhttoken)
curl --oauth2-bearer "$token" \
    -Fcontent=@site.tar.gz \
    https://pages.sr.ht/publish/blog.jaxter184.net
