+++
title = "MIDI Sucks: A Rant"
slug = "midi-sucks"
date = 2023-04-01
updated = 2023-04-01
[taxonomies]
tags = ["thoughts", "hardware", "music", "twitter archive"]
+++

The following is an archive of a Twitter thread I made on January 12,
2019.

MIDI is a garbage standard that needs to be replaced in order for
the music production and performance world to progress. It's modern
development basically consists of workaround after workaround to try to
make something that vaguely resembles a usable format.

The only thing its good at is boiling down keyboard instrument
data. Literally any instrument that doesn't consist of a
black-and-white set of clavier keys cannot be fully expressed using
MIDI. For example, drums make different sounds depending on where and
when you hit them.

MIDI also doesn't have enough data precision to represent a lot of
human expression. Most notably, they had to split pitch bend control
into two MIDI messages. If that's not kludging it, I don't know what
is.

Furthermore, General MIDI is weak. It did well up until the early 2000s
because pop music had a very limited use of synthesis. Everything
was basically either a saw/squ lead/pad. However, with the surge in
electronic influence on pop music, this is no longer the case.

127 is a very limited number. There are more than 127 types of musical
sounds in the world. Heck, the number of percussion sounds alone
exceeds the 127 limit of MIDI. Also, 7 bits? Really? Are we gonna live
in 2019 and still not use a power of 2?

OSC is the obvious alternative, but it lacks a crucial feature that
made MIDI ubiquitous: standardization. Because OSC doesn't have a
rigorous standard, it can't really compete, which, in its defense,
was intentional. MIDI's rigidity is what will (hopefully) lead to its
demise.

Another crucial downside of OSC that I forgot to mention is that it
doesn't have a file format. It kinda makes sense due to the fact that
the messages aren't standardized, but if it's gonna overtake MIDI,
having a way to save streams of data to read later or edit is a must.

Not to beat a dead horse, but another thing MIDI lacks that I forgot
to mention is that it can't send multiple messages at once, making
it pretty annoying and unpredictable when using it to stream live
sequenced music data.


# Replies

@strobemusic said: I think it works just fine...

![photograph of someone's studio, with a Roland TR-808, Akai
MPC 3000, Prophet V0, a mixer board, and lots of rack-mount
synthesizers](https://pbs.twimg.com/media/Dwv2Ye0UwAEIB00?format=jpg)

My reply:

I think this picture is actually a very good argument against MIDI. In
the center of your image is the Roland TR-808, which is quite possibly
the most iconic electronic instrument ever, despite not using MIDI at
all in any component of its functionality.

In addition, most of the stuff on 19' racks don't use MIDI as
far as I can tell, and it would also be physically impossible for
your mixer board to be MIDI compatible because it has more than
16 channels. Ultimately, MIDI was meant for a very specific thing:
keyboard instruments.

I think of all the gear in that picture, the only thing that requires
MIDI to function at a basic level is that red synth in the top
right. Sure, some of the things in that picture can send and recieve
MIDI, but I assuming you don't use the MIDI functionality of those
keyboards.

Addendum (2023-091): The 'M' in "MPC" literally stands for MIDI, so
that product could also probably be classified as one that "requires
MIDI to function at a basic level".
