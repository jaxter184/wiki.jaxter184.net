---
title: Gedanken über Trackballs
date: 2019-10-18T15:06:40-05:00
taxonomies:
  tags: [Gedanken, Geräte]
---

# Vergleich gegen Maus

## Vorteile:
+ Sehr platzsparend
+ Kann man auf jede Oberfläche benutzen. Ich oft platziere meiner
Trackball auf mein Bein, oder auf die Fensterbank des Bus. Auch meiner
riesiger Trackball ist tragbarer als meisten tragbare Mäuse.
+ Erfordert man nicht viel Armbewegung. Dvorak ist auch aus diesem
Grund gut.
+ Mehr Daumenverwendung. Der Daumen ist das stärkste Finger, und meiste
Mäuse nicht nutzen der Daumen.

## Nachteile:
- Der Kugel ist seltsam.
- Es gibt mehr Arten von Mäusen als Trackballs.

# Vorschläge (Emplehlungen?)

### [Elecom HUGE](https://www.elecom.co.jp.e.gj.hp.transer.com/products/M-HT1DRBK.html)

Der bester Trackball, das ich getestet habe. Bessere Ergonomie, mehr
Funktionstasten, und besser Scroll als der Kensington Expert. Ich liebe
Scrollringe, aber es ist ungewohnt zu nutzen. Gibt es in kabelgebunden
und kabellosen Arten.

### [Elecom DEFT PRO](https://www.elecom.co.jp.e.gj.hp.transer.com/products/M-DPT1MRXBK.html)

Ich benutze diesen Trackball mit meinem Laptop. Die Bluetooth-Funktion
ist sehr praktisch. Es auch hat einen drahtlosen USB-Dongle und ein
USB-Micro-anschluss. Es auch hat eine Funktionstaste weniger als der
Elecom HUGE.

<!-- my favorite thing about it is that it
allows for wired USB, wireless USB or Bluetooth in the same model, as
opposed to the Elecom HUGE, which has two separate models for wired and
wireless, and no Bluetooth on either. -->

### [Logitech M570](https://www.logitech.com/en-us/product/wireless-trackball-m570)

<!-- A good entry level trackball. I personally don't like thumb balls,
but all of my friends seem to prefer them over finger balls, possibly
due to the positioning of the left and right click. Finding left click
seems to be the first issue that people have when they try to use my
trackballs. I personally think it only takes about 30 seconds to get
used to the positioning on a finger ball, but mileage may vary. -->

### [Logitech MX Ergo](https://www.logitech.com/en-us/product/mx-ergo-wireless-trackball-mouse)

Hab ich nicht benutzt. Ist sehr teuer.
<!-- I have never owned or used one of these, so I can't officially endorse
it, but it seems to be the thumb ball equivalent of the Elecom DEFT
PRO. Very pricey though. -->
