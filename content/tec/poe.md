+++
title = "A Plea for PoE"
slug = "poe"
date = 2023-03-30
updated = 2023-03-30
aliases = ["poe"]
[taxonomies]
tags = ["thoughts", "hardware", "music"]
+++

<!--
## First, a bit of speculative imagination

In my ideal future, I imagine an eletronic musician on a stage with
about 4 other bandmates, each with various instruments, almost all of
which are not invented at time of writing. The lead musician takes the
stage, with an instrument made of aluminum and plastic, that looks a
bit like a hybrid between a bass guitar and a saxophone. At first,
it may seem like a bit of a gimmick, intended to distinguish this
particular musician from the many others, but after they start playing,
you realize: it's not the instrument that distinguishes the musician,
but the musician's ability that instead makes the instrument shine
far brighter than the many other instruments that exist. As you see
them shred their solo to bits, you can't help but grin, appreciating
everything that had to happen to put this person in front of you,
conveying the whole of their creative spirit through their musical
perfomance.
-->

At risk of sounding like I'm gatekeeping, I think that ever since
music festivals became the primary medium for electronic music, live
performance has become more about the giant spectacle of screens,
lights, and fire. The musician becomes a mere speck on the stage,
only there to make the audience feel like they're not just paying for
a pre-programmed lightshow and a playlist[^1]. CDJs and laptops lock
the performer to one position, and limits the dynamism and physicality
of their performances. Heck, it's called Electronic \*Dance\* Music,
but I've never seen a DJ do much more than bob their head, pump their
arms, and clap their hands (TODO: write an article about how throwing
a cake should be considered a dance move). It's great that electronic
music has given lighting and effects operators the freedom to unleash
their craft on the world, but I can't help but compare these shows to
rock concerts and drum circles, and wonder: what could it look like if
today's instruments were better at expressing today's music?

To emphasize: it is not the music, the musician, or the stage that
is in need of reform. There is plenty of musicianship and creativity
in electronic music today; arguably more than there was for rock and
classical. It's just that the limitations and hassle of the hardware
restrict how that music can be performed. Perhaps a laptop is effective
and convenient enough for the \*production\* of music, but I don't see
a future in laptops as a tool for \*performance\*.

<!--
My argument is that the primary limiting factor is the lack of a good
communication channel.

Most non-stationary instruments, like handheld
lutes ([321.3![wikipedia link][wl]](hs321)), non-free aerophones
([42![wikipedia link][wl]](hs42)), and electric guitars, require only
one cable between the instrument and the sound system (usually XLR
or TRS/TS).

A particularly complicated signal path for an electric
guitar might be:
```
[guitar] -TS-> [pedals] -TS-> [DI box] -XLR-> ...
```

while for a MIDI controller (not a standalone synthesizer) at its
simplest would be:
```
[controller] -USB-> [laptop] -USB-> [audio interface] -XLR-> ...
```
-->


## Anecdote of a musical stage perforance

I recently helped out with a few showcases for a local multimedia
festival. One such showcase was run by a record label, and they brought
in a bunch of musicians of various genres, and they had to go on one
after another. Some bands had 5+ members and drum kits, while one had
just a laptop and a microphone[^2]. Now, between the two, you would
think that the laptop would be a lot easier, right? Well, technical
difficulties with the laptop put the whole schedule back almost an
hour. A dongle was lost at some point, cables had to be switched out,
devices had to be restarted; a culmination of Murphy's Law. And this
was on top of the already cumbersome setup process of taking each
device out of a suitcase and plugging in the various power and USB
cables.

I can't guarantee that the following proposal will solve all of this,
but if there's anything I've learned from those last few days of
stage work, it's that electronic music performance is in dire need of
simplification. In its current state, using a laptop on stage with its
tangle of cables going to MIDI controllers, audio interfaces, and power
supplies is a complicated problem, solutions for which are very fragile
(compared to setups for other types of music performance). My goal
with this manifesto is to improve technology in order to simplify the
experience for electronic musicians performing their music.


## Crash course on PoE

[Power over Ethernet (PoE)![wikipedia link][wl]][poe-wiki] is a set of
networking standards designed to allow for power delivery over (you
guessed it) an Ethernet connection. Similar to [USB-PD![wikipedia
link][wl]][usbpd-wiki], the devices use the protocol to negotiate
power, and both sides have to be compatible with the standard.


## PoE vs USB-PD

|            |Ethernet|USB 3.0|
|------------|--------|-------|
|Use cases   |Limited to internet networking, and a small handful of other niches (one of which we will come back to later)|Very flexible, and near-universal (which seems to have been the goal, based on the name).
|Speed       |Cat 5e only allows for speeds up to 1 gigabit per second (but Cat 8 allows for up to 80 gigabits!)|At time of writing, can go up to 20 gigabits per second with compatible devices and cables.

|            |PoE|USB-PD|
|------------|---|------|
|Availability|Power usually comes from a switch, and PoE switches aren't abundantly common outside of IT and commercial networking solutions (though PoE switches seem to be more common than USB-PD hubs)|Most laptops come with USB-PD chargers at time of writing, and if a battery bank has a USB type C output, chances are, it supports USB-PD.
|Power       |802.3af (Base-level PoE) nominally supports 15.4 watts, while 812.3bt (PoE++) supports up to 90 watts <span style="opacity:0.5">[[1]](#1)[[2]](#2)</span>. Usually runs at 48 volts <span style="opacity:0.5">[[3]](#3)</span>.|Boasts over 180 watts at 36 volts.
|Impl*       |Requires IC for rerouting the connections depending on whether PoE is negotiated|Doesn't necessarily require a dedicated chip, only that your microcontroller has peripherals for USB-PD negotiation.

\*: It's a bit tough to say authoritatively which is easier to implement
from a hardware development perspective (focusing on the receiving
end), as I've never implemented either. My understanding is that
in USB-PD, the power arrives through dedicated conductors, while with
PoE, the power comes in on the same twisted pairs that data could
come in on. No citations for anything said in this row; it's all
guessing and speculation. I will hopefully be able to update it soon
after trying some stuff out and doing more research.


## So why PoE?

Despite USB being a better standard for 90% of applications, there is
still a reason that Ethernet dominates the networking industry. The
two main benefits that I can see are: cables are extremely cheap (and
easy to crimp yourself), and can get extremely long. With USB 3.0
(using 26 AWG wire), you get approximately 3 meters maximum <span
style="opacity:0.5">[[4]](#4)</span>. On the other hand, Ethernet
cables (of any Category) are generally rated for 100 meters. The extra
length is \*absolutely vital\* for making digital music controllers
more portable. With USB, the musician is tethered to a circular area
with a radius of 3 meters, but with Ethernet, the laptop doesn't even
have to be on the stage. It could be setup somewhere out of the way
and left alone between performances, which would have almost certainly
prevented the issue with the laptop in the anecdote from earlier. Or
you could go as far as to have a rack-mount desktop, connect it to
a PoE switch, and run a single ethernet cable to each musician and
move around with the same freedom that an electric guitarist might
have. Just one cable between the musician and the sound system.

While I don't know of any hardware that uses PoE specifically,
there are plenty of open standards for [audio over an Ethernet
connection![wikipedia link][wl]][aoe-wiki], often used to send many
channels of audio over a "digital snake" from a stage to its sound
booth and back. It isn't strange to see a roll or two of Cat 5(e) cable
in a live venue's arsenal.

Some Pioneer CDJs and mixers are equipped with Ethernet for a feature
that they call [Pro DJ Link][pdjl]. Among other things, it connects any
attached filesystems to other devices on the network, and can send the
audio to a laptop (also on the network) with the appropriate software.
I only know about this because during the aforementioned showcase,
anytime there was a DJ accompanying the vocalist, the CDJs and mixer
would be brought on stage in their flight case, (along with a Wi-Fi
router and power strip), and setup was as easy as just plugging in the
power and letting the booth take it from there. It went without a hitch
every time.

The current state of DJ hardware connectivity is the future that I see
for all music hardware: controllers, audio interfaces, rack-mount
synthesizers, effects processors, eurorack, and anything other
electronic device that can be used to make or perform music, all
connected to one PoE switch offstage and nothing else.


## Enter: Open Sound Control

By reading this far, you have just activated my trap card. For the past
3 years or so, I have slowly been coming to the conclusion that [Open
Sound Control (OSC)![wikipedia link][wl]][osc-wiki] is the solution to
almost all of my musical problems. Voice synthesizer plugin needs some
way to receive phoneme info? OSC has character and string types. Want
more semantically useful numeric types in a tracker-based DAW? OSC
has single and double precision floats, 32 and 64 bit integers,
booleans, infinity, timetags, and nil (if you count that as a numerical
value). Need a file format that can express sample-accurate timing
and namespacing? It's not standardized, but `oscdump` has a pretty
reasonable output, and if that's not to your liking, you can just
store the raw bytes, since time information is encoded in the packet
(along with the message and its namespace, of course). Of course, OSC
has its own drawbacks, so if you can do something entirely with MIDI,
it's probably best to do it with MIDI. But if you need the length of an
Ethernet connection anyway, you might as well use OSC as the transport
layer for your MIDI data.

One of the notable things about OSC is that its designed to run
on top of a network. Most OSC-compatible software asks for things
like IP addresses and ports, and can communicate with OSC endpoints
over the internet. OSC was made for Ethernet, and with the locking
clip feature of RJ45 connectors (or optionally Ethercon), longer
maximum cable length, and industry adoption of audio over Ethernet,
I predict that PoE music hardware will be the ["killer app"![wikipedia
link][wl]][killer-wiki] of OSC.


## Closing thoughts

Please let me connect the music half of my 19" rack to the networking
half.


# Footnotes

[^1]: Don't get me wrong, a good light operator makes the show worth
watching; [ghostdad] is at least 20% responsible (a conservative
estimate!) for the success of Worlds and its public perception as a
[Gesamtkunstwerk![wikipedia link][wl]][gesamt-wiki] rather than just
an album. Also, CDJs can totally be the right instrument for certain
situations! Small sets at showcases and conventions, and places like
Boiler Room come to mind. If you want music at your party, then a DJ is
the perfect musician, and CDJs are the perfect instrument. Plus, some
musicians don't \*want\* to learn to play an instrument, but still want
to be involved in the presentation of their music.

[^2]: One performer even brought his own hardware rack-mount vocal
processing device, complete with a dedicated technician/operator,
who stood at the side, just offstage and spent most of the performance
turning a single knob (presumably in a very particular or technically
demanding way, otherwise he wouldn't have been flown out to do it)

# References

<a id="1">[1]</a>
https://www.netgear.com/business/solutions/poe/overview/

<a id="2">[2]</a>
https://www.tp-link.com/us/solution/poe/

<a id="3">[3]</a>
https://superuser.com/questions/1105242/how-many-volts-there-are-in-poe

<a id="4">[4]</a>
http://janaxelson.com/usb3faq.htm#ca_maximum

[wl]: /images/wikipedia.svg
[poe-wiki]: https://en.wikipedia.org/wiki/Power_over_Ethernet
[usbpd-wiki]: https://en.wikipedia.org/wiki/USB_hardware#USB_Power_Delivery
[gesamt-wiki]: https://en.wikipedia.org/wiki/Gesamtkunstwerk
[pdjl]: https://support.pioneerdj.com/hc/en-us/articles/4405902925593-What-is-the-PRO-DJ-LINK-function-
[aoe-wiki]: https://en.wikipedia.org/wiki/Audio_over_Ethernet
[osc-wiki]: https://en.wikipedia.org/wiki/Open_Sound_Control
[hs321]: https://en.wikipedia.org/wiki/Hornbostel%E2%80%93Sachs#Lutes_(321)
[hs42]: https://en.wikipedia.org/wiki/Hornbostel%E2%80%93Sachs#Non-free_aerophones_(wind_instruments_proper)_(42)
[killer-wiki]: https://en.wikipedia.org/wiki/Killer_application
[ghostdad]: https://ghostdad.world/
