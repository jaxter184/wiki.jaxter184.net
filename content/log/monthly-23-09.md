+++
title = "Monthly Log 2023-09"
slug = "monthly-23-09"
date = 2023-09-30
[taxonomies]
tags = ["monthly", "webdev"]
+++

# Updated [contact page]

[contact page]: https://contact.jaxter184.net

It's been almost exactly one year since I made a contact page for
collecting all the different public online spaces where I exist.
I had a weird experience the other day where I joined a Discord
channel, and someone there used `contact.jaxter184.net` to find my
SoundCloud account, and then posted my music in the Discord channel.
At least, I assume they did, because I have it linked in my Discord
bio, though it's possible they just searched "jaxter184".
But even if they didn't use my contact page in this particular case,
I like the idea of making it easier for these disparate parts of my
online life to be more connected, especially in this age where social
media platforms seem to be fragile and temporary.

As part of this refresh, I decided to change my profile picture.
I've been using my old one for several years, but I've recently changed
the way I'm doing my hair (and it will probably change again in the
near future), so the old one felt outdated.
Previously, I had used Blender to render all the fancy shapes hanging
around my head, but I wanted to be a little more reserved in the new
one, so I shrunk the hexagon and used Inkscape.
This has the side-benefit of being more web-friendly in the few places
that I can use an SVG file.
I also had an SVG version of the old profile picture that I made for a
much older website, but I seem to have lost it.

<table>
<tr> <th> Old </th> <th> New </th> </tr>
<tr>
<th>
<img src="/images/pfp-render.jpg" alt="old profile picture, titled
*Render*. A digitally painted, traced image of jaxter184 with short
hair. A wide bright green (#20ff80) curved irregular, but symmetrical
hexagon covers the contour of his face. A glossy cyan tube loops twice
around his head, and a collection of small matte orange icosahedrons
float." width="360">
</th>
<th>
<img src="/images/pfp-trunc.svg" alt="
new profile picture, titled *Gradient*. A vector depiction
of jaxter184 at a roughly 3/4 angle. a beige-skinned figure,
head tilted to the side, with hair tied up in a messy bun. A
bright green (#20ff80) hexagon barely covers his facial
features." width="360">
</th>
</tr>
</table>

Frankly, I think I prefer the old one.
The sheen on the face covering is neat, and there's a bit more whimsy
in the loops around my head and the little orange icosahedrons.
That being said, I'm pretty happy with how the gradients look in the
new one, and it's certainly a more accurate depiction of what I look
like now.
Hopefully I'm a bit quicker to replace this new one than I was to
replace the old one.

# Attempted to debug probe.rs

A friend reached out to me to try and do some Matter IoT stuff
recently, and this is the chip he wanted to use.
So far, I've generated a peripheral access crate from the SVD files,
and verified that it works by using Silicon Labs' Simplicity Commander
tool to flash the ELF file.

However, the tool I use for most of my embedded development is
`probe-rs`, and I thought it would be nice to try and add support for
the particular chip I'm using.
They have support for some other Silicon Labs chips already, so how
hard could it be to add support for this one?

[pause for dramatic effect]

I've had [an issue simmering in their tracker][probe-issue] for a while
now, and they've basically held my hand through the debugging process.
To be clear, the reason this is so difficult and taking so long is due
to my incompetence (if it wasn't clear from the issue discussion).
At this point, I'm considering just buying a dev board for Yatekii and
asking them to figure it out.
Would probably be faster and easier on their side rather than trying
to remotely debug through a lossy, flawed communication channel
(i.e. yours truly).

<!--
[^1]: peripheral access crate: a sort of low-level, typed,
memory-unsafe API for modifying bare metal registers
-->

[probe-issue]: https://github.com/probe-rs/probe-rs/issues/1700

# Started Tone Tram

I have ideated yet another digital musical instrument interface to
add to my pile of half-complete digital musical instrument interface
pile.
I am nothing if not consistent.

This one is basically an [Ondes Martenot![wikipedia
link][wl]][martenot], but digital.
Pretty funny coincidence that "Martenot" backwards is "tone tram",
since this thing is basically a little train that goes back and forth
to control the pitch of a tone.

[link to post](/prj/tone-tram)

[martenot]: https://en.wikipedia.org/wiki/Ondes_Martenot

# Started crofu

I've procrastinated on implementing [ICC](/prj/icc) for quite a while
now.
I think at this point, with the help of the people who have been kind
enough to talk about it with me, I've developed the idea enough that
I'll learn the most from simply executing it.

And so, behold: [crofu](/prj/crofu)

<!--
# Upgraded desk

TODO: images

added mic stand

added ring light
-->

# Wrote my first monthly log

It remains to be seen how consistent I will be with these, but at the
very least, I think it helps with the blog-writing process.
It's very rare that I'll "finish" a project, so focusing on the little
things that I've done this month might be a better way to exercise
my blogging muscles, and make it easier to write the more interesting
articles on a given project.
I'm also planning on making project posts more "live", so that they change
as I progress through the project.

There are a couple different goals of this blog, but one big one
is to share the progress I've made.
Developing on an idea is a slow process riddled with obstructions, and
maybe by sharing my experiences, someone doing something similar in the
future will be able to move more effectively through the problem space.
Of course, no matter how much you learn about a subject, there will
always be issues, so I'm not too worried about depriving someone of the
joy of figuring things out.

[wl]: /images/wikipedia.svg
