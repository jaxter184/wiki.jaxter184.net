+++
title = "Monthly Log 2023-10"
slug = "monthly-23-10"
date = 2023-11-01
updated = 2023-11-02
[taxonomies]
tags = ["monthly", "webdev"]
+++

# Card game?

Two of the folks on labyrinth.social [came up with an idea for Mastodon
trading cards][thread].
The idea immediately roused my interest, perhaps because I've been
very loosely dabbling in game design on and off for the last few months
(running a DnD campaign, trying out [Bevy]).
While there are plenty of services online that will print playing
cards for you with arbitrary designs on both the front and back, if
there's one thing I hate, it's spending a reasonable amount of money
for something convenient and high-quality when I could instead spend
even more money and time and get a far inferior result.
So after spending about $30 on paper, glue, and varnish, and $150 on a
used laser printer, I've come up with a vague process for creating some
cards that look and feel slightly better than if you simply printed
them out on regular paper with a regular printer.

Next step (honestly, this step should have come before the huge money
sink) is to do a few mockup designs and develop some game rules.
I ideated a few concepts when I initially came across the concept, but
I've also jotted down a few notes, which were as follows:

[thread]: https://social.linux.pizza/@jaxter184/111055809417501143
[Bevy]: https://bevyengine.org/

```markdown
the goal is not necessarily to win, its to:

* have fun
* communicate
* learn
* connect
* interact
* commune
* self-express

though winning is also desirable

* there should be a "game mechanic". perhaps dnd-esque? in that it is
collaborative rather than competitive
* ooh, maybe like a multiplayer solitaire? or
https://en.wikipedia.org/wiki/Set_(card_game)
* even if there are no winners and losers, there should still be a
meaningful result.

* in my imagined world, people carry these cards around like business
cards and exchange them
* there should be no reason to counterfeit them. they should only have
meaning because someone you know gave them to you.
* put nfc chips in them?
* each instance should have a different back, and maybe a front frame style
```

This is in a very very early state, but hopefully it develops into
something meaningful!


# Re-entering the world of papercraft

I also made a little papercraft to test the printer, and I think I'm
gonna do more:

<table>
<tr>
<th>
<img src="/images/papercraft-house-side-edge.jpg" alt="photo of a
little papercraft house in a sort of Tudor Revival style, with a
stone first floor, wooden door, and stucco second floor with wooden
beams. very simple shapes, basically a cube with a triangular prism
roof. first floor is very slightly inset from the rest of the house. It
is textured in a pixel-art style" width="360">
</th>
<th>
<img src="/images/papercraft-house-top-corner.jpg" alt="photo of the
same house as previous, but from a different angle" width="360">
</th>
</tr>
</table>

The toner sort of rubbed off a bit, and I believe that has to do with
the printer settings, rather than the actual quality of the printer or
toner or paper.

The model itself is based on ["Pixel House"][house], by
[tacocrepe][crepe], licensed under CC-BY 4.0:

<iframe title="Pixel House" frameborder="0" allowfullscreen
mozallowfullscreen="true" webkitallowfullscreen="true"
allow="autoplay; fullscreen; xr-spatial-tracking" xr-spatial-tracking
execution-while-out-of-viewport execution-while-not-rendered web-share
src="https://sketchfab.com/models/fb19698f540649d58d840e7e3459c9a6/embed?ui_theme=dark"
width="100%" height="540px">
</iframe>

[house]: https://sketchfab.com/3d-models/pixel-house-fb19698f540649d58d840e7e3459c9a6
[crepe]: https://tacocrepe.carrd.co/

Papercraft tools have come a long way since when I was last involved in
the hobby. Back in my day, all we had was the free demo of Pepakura, or
[papertoys.com](https://www.papertoys.com/) (Sydney Opera House was my
favorite, and Statue of Liberty was my least favorite). But now, there's
a Blender plugin!

In the first place, I tried to pick one that would be quick and easy to
make, but I ended up feeling the need to edit it anyway. After learning
the shortcut for adding an edge through a face based on two of its
vertices ([J][cut]), I was able to make the changes that I wanted to,
but frankly, I think I should've just left it alone, because it looks a
lot less cool with my changes.

[cut]: https://blender.stackexchange.com/questions/238505/how-to-divide-a-face-in-two


# Researched wikis

I stumbled upon [Maggie Appleton's history of Digital Gardens][gardens]
on Mastodon, and while it has some insightful observations on the
nature of personal wikis and how they interact with the rest of the
internet, honestly, all it took for me to get on board with the idea
was simply the word "garden".
It feels so much more in line with the kind of thing that I want to
make than "blog".
So what are the steps I need to take to convert this blog to a garden?

Initially, I thought the move would be disruptive.
I was looking up wiki engines, and considering maybe even maintaining a
separate wiki and blog.
[ikiwiki] stood out as a particularly interesting engine, and prided
itself on its configurability, and the fact that you could also use it
as a static blog generator.
Tragically, it is written in Perl.

[ikiwiki]: https://ikiwiki.info

But running into ikiwiki made me realize that maybe a blog and a wiki
are two different shapes of the same material?
If I could just add some extra navigation elements and link more
aggressively between pages, then I'd be 90% of the way to a wiki.

That being said, there are some features
-- like automatically styling external links and streamlined editing
without logging in, or limiting edits to people who are (perhaps using
IndieAuth) --
that I would like to have, but would require significantly more work
than simply making a few minor changes to my Zola theme.

Like with the previous project, I haven't made any meaningful steps
toward actually doing anything. But I'm excited!

Also, recently, [one of my favorite 'Lu's][lu0] got in contact with
[my other favorite 'Lu'][lu1] and recommended that they cultivate a
personal wiki.
The [Hundred Rabbits log] is what inspired me to start recording these
monthly log entries in the first place, so it's cool to see someone
who I really respect directly influencing someone else who I really
respect.
Especially when I'm being influenced in the same way!

[gardens]: https://maggieappleton.com/garden-history#a-brief-history-of-digital-gardens
[lu0]: http://wiki.xxiivv.com/site/devine_lu_linvega.html
[lu1]: https://www.todepond.com/wikiblogarden/my-wikiblogarden/
[Hundred Rabbits log]: https://100r.co/site/log.html

## Additional reading

* <https://j3s.sh/thought/my-website-is-one-binary.html>
* <https://grimgrains.com/site/home.html>
* <http://notebook.zoeblade.com/>

# Refactored [tlature]

[tlature]: https://git.sr.ht/~jaxter184/tlature

While it may seem like I'm starting tons of projects and not actually
working on my old ones, it's actually the case that the single
non-sleep activity I spent the most time doing the last 32 days (16.1%
according to my logs) was working on tlature.
I set a bit of an artificial deadline for myself as I approached the
start date of my new job to get as much work done on it as possible
before I started.
While I had hoped to get to a point where it was usable by someone
other than myself to make music, I don't think it's quite there yet.

The current plan is to do a more detailed writeup of what I actually
did once I get Zola set up for a more wiki-like workflow, but in a few
words, I:

* Refactored routing
	* Block is now a chain of processors rather than just one
	* User-defined routing is now its own processor rather than
	something built into the Block
* Revamp how the view modes behave; everything is now nested
* Refactor `Tracker` as a processor <!--so you can have them anywhere
you can have any other processor, rather than operating on one global
`Tracker`. I will likely regret this in the near future, but the main
motivation was to make it easier to turn the `Tracker` into a plugin in
the future.-->
* Refactor how commands work <!-- why? -->

As you can see, it was mostly refactoring, and there were very few new
features.
In fact, I'd say it's probably less capable now than it was at the
beginning of the month.
Even so, I think overall, this is a good result, and has primed the
codebase for future features.
Unless my whims shift once again and I do another huge refactor.


# Wrote my second monthly log

I was about 23 hours late to this month's post, but frankly, the fact
that I did it at all is something that I'm proud of.
It could have very easily been the case that I simply forgot to make
one.
In fact, it wasn't until about 2 hours ago that I even remebered this
was a thing that I was doing.
Maybe I should set an alarm or a calendar event to remind me.

In terms of the actual log content, I feel like I'm still having
trouble with the actual writing part;
a lot of my sentences are a bit clunky, and it's tough to communicate
what I want to, in the way that I want to, but also make sentences flow
naturally into each other.
But such problems are exactly the kind of thing that I'm hoping to fix
by making these entries, so I'm glad that I'm at least noticing the
shortcomings.

I think the core to this is going to be to better understand why people
read blog posts (and wiki articles), and what I can be doing to make my
writing more appealing for those people.
So if you've made it this far, [please reach out] and let me know what you
think!
Or even just let me know that you've made it here.

[please reach out]: https://contact.jaxter184.net

[wl]: /images/wikipedia.svg
