+++
paginate_by = 12
sort_by = "weight"

extra.after_dark_menu = [
    {url = "$BASE_URL", name = "Home"},
    {url = "$BASE_URL/tags", name = "Tags"},
]
+++

# blog - jaxter184 - training to become a full-stack musician

## monthly logs

* [2023-10](/log/monthly-23-10)
	* fedi card game
	* papercraft
	* wiki research
	* tlature refactors
* [2023-09](/log/monthly-23-09)
	* update contact page
	* [tone tram](/prj/tone-tram) (mechanical digital theremin?)
	* [crofu](/prj/icc/crofu) ([incremental converse crowdfunding](/projects/icc) platform)
