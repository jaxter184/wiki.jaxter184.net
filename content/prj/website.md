---
title: This website
date: 2019-12-01
updated: 2023-11-05
taxonomies:
  tags: [project, software, web]
---

## Why make a website?

I've got quite a rocky relationship with information.

The way the internet is structured makes it very easy for an individual
to broadcast their voice to others. Most people use social media for
this type of thing, but most of my favorite places to read about what
people are up to are blogs. The straightforward, yet flexible structure
of a blog post seems to be to be the most clear and easy way to express
ideas over the internet. Though videos and podcasts are also a great
way to convey information, they take much more time and energy to
produce than a blog post.

## How make a website?

Make a text file, put whatever words you want in it ("Hello World!" are
some popular first words), and put `.html` at the end of the filename.
Congratulations! You've made a website!

Of course, this lacks many of the features widely considered important
for most websites, such as being connected to the world via the
internet. But one of the things that I remind myself whenever working
with websites is that, like many other forms of art, you can put
exactly as much into the medium as you want, [even if that amount is
practically nothing](https://en.wikipedia.org/wiki/4%E2%80%B233%E2%80%B3).

<!--
this is maybe too simple

Should you decide you want to add more to your website, one of the
first things you're likely to run into is HTML. As a kid, one of my
first interactions with programming was making a neat little website
with ordered and unordered lists, and headers, and music. You can get
pretty far with just HTML! (and maybe some CSS to make things look
pretty)
-->

The first iteration of this website was made using Jekyll, a static
website generator. I basically pick a theme, write some markdown files,
and boom, website. I had some issues getting the GitLab pages pipeline
to work properly, and some redirection authentication shenanigans,
but overall, this has been a much smoother and simpler process. I like
this a little more because it lets me focus on the actual content of
the site rather than the minute details of how everything is going to
look. Going forward, I'm definitely going to write my own Jekyll theme,
once I figure out how.

## Zola

Just kidding, dont use Jekyll. It's very cryptic and confusing compared
to Zola.

## wiki/garden?
