+++
title = "Incremental Converse Crowdfunding"
slug = "icc"
date = 2021-12-24
updated = 2023-09-25
sort_by = "date"
aliases = ["icc"]
[taxonomies]
tags = ["thoughts", "economics", "icc"]
+++

I've become a lot less confident in this idea recently, and am planning
on once again rethinking it. I'll probably continue to aim to use it in
a small scale on a personal project, but with the intention to totally
scrap it if anything goes unfixably wrong.

<!--
* information-based products are practically free to copy and distribute.
* The process of making it, however, still takes a lot of time and
energy, and chances are, you don't make a ton of money from it, or
you've sold your soul to make something that you don't really like
working on.
* What you do full-time to pay the bills is probably unrelated to
the things that you actually want to do and are good at.
* It could be better for you, and arguably society as a whole, if you
could be financially compensated for your work.

It would be ideal if "good" works were compensated proportionally.
That is, if an audience recognizes an artwork
to be a meaningful contribution to the world of art,
or the users of some software find it to be very easy and fun to use,
then the people who make them should generally make more money
than they would if that were not the case.

However, these creators should also be able to choose
how they do their work,
as their expertise means they have a better understanding of
what does and doesn't work, and what the likely outcomes are.
-->

`Incremental Converse Crowdfunding` is a system that attempts to make
donor intent easier and more precise.
It works by the following steps:

* The artist publishes a list of proposals for works that they are
willing to produce (a chapter of a comic, a sound design tutorial,
software feature, etc.)
* Some of their donors make pledges towards each of these proposals.
The amount they pledge is based on how much they would like for the
artist to complete each work.
* These pledges are weighted and combined, then presented to the
artist as estimated payouts that they would receive for publishing the
proposed work.
* The artist produces and publishes the proposed work.
* Each donor is invoiced the amount they pledged, with the option to
donate more (or less), based on their expectation when they made the
pledge, and how that compares to the final quality of the work.
* Donors who donate less than they pledged are penalized in their
future pledges via the scoring system
(i.e. their pledges are scaled down during the weighting and combining
process, but they are still expected to donate the full value of their
pledge).

I use the term "artist" when talking about the person producing
works, but as mentioned, this also applies to people who produce audio
or software or any other information-based output that can be trivially
copied and distributed.

## goals

* maximize the quantity and quality of works entering the commons
(i.e. encouraging reuse and collaboration using FOSS and Creative
Commons licenses)
* give donors a reason to pledge (and donate) to help make your
creative output possible
* encouraging artists to provide something that their donors actually
want *without* sacrificing artistic autonomy
* distribute resources in an ethical and just way


# Problems with traditional crowdfunding

I believe the biggest problem with traditional crowdfunding is that the
final result is often far less than what is initially pitched.
Searching for "failed kickstarters" provides plenty of listicles
outlining Kickstarter campaigns and why they failed, and the various
reasons why:

* the people running the campaigns were scammers
* the people making the product severely overestimated their abilities
* unforseen legal or logistical obstacles
* all of the above

In addition, once the funding is provided, there is little reason for
producers to be expedient in their delivery
(other than their usually nonexistent reputation),
and it is difficult, if not impossible, for funders to get their money
back if the producer takes too long or disappears.

To fix this, I have two proposals:
* **converse** crowdfunding: flip traditional crowdfunding so that the
money leaves the funder's hands after the final result is presented
* **incremental** crowdfunding: campaigns should be split up into
smaller pieces, with incremental goals that give the opportunity for
funders to recalibrate their excitement in the project as a whole

By combining these two concepts, I have come up with the procedure
outlined at the beginning of this article.


# Converse crowdfunding

The traditonal method of crowdfunding can be described by the statement
"If I fund this, then you will produce it".
The [logical converse](https://en.wikipedia.org/wiki/Converse_(logic))
of that statement would be "If you produce it, then I will fund this".
In converse crowdfunding, donors pledge money towards a work, and only
donate after publication of that work.
This gives artists an incentive to deliver a work quickly, but with
minimal sacrifices to quality.

<!--
[^1]: Just for fun, inverse crowdfunding would be "If I do not fund
this, then you will not produce it", and contrapositive crowdfunding is
"If you do not produce it, then I will not fund this".
-->

# Incremental crowdfunding

Splitting up a work into smaller tasks gives donors more opportinuties
to predict how much they value the final result, and increase or
decrease their pledges if their expectations change.
By providing donors with information more often, they can make more
informed decisions about how much they want to pledge for a given work.

Similarly, artists get more insight into how their work is perceived,
as their donors give regular feedback on each work via their donation
amount.
In contrast, metrics from social media can be very volatile, and are
just as often due to randomly being shared by the right person at the
right time rather than a conscious, attentive evaluation.
Plus, $2 is usually a more meaningful show of support than a social
media like.

For most cases, works can be easily split into smaller parts.
For example, a music album could be split into
3 promotional singles,
album art,
a full release,
5 music videos,
an extended/collector's edition release,
and a remix album.
Serial works like TV shows and comics could be split into
episodes and chapters,
or even further into distinct production steps
like character references, screenplays, and storyboards.

Because no money is transferred until after the work is published,
artists are essentially working off their own money for any given work.
As a result, artists are encouraged to split their work into definable
milestones, and be as transparent in their progress as possible.
In this way, incremental crowdfunding follows as a natural outcome of
implementing converse crowdfunding, and so it isn't really necessary to
force works to be split in this way; only to allow and streamline it.

<!--
# crofu

crofu is a (in progress) implementation of the ICC paradigm. The
platform will start out being me and perhaps a few people I know, and
then expand to anyone producing FOSS or Creative Commons works.
-->


# Additional notes

* [Prior art](../icc-prior-art): Patreon, Bountysource, Liberapay
* [Extra features](../icc-continued): Donor Reliability Coefficient,
social engineering
* [Potential concerns](../icc-concerns): too many to list
