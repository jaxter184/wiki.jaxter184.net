+++
title = "crofu"
slug = "crofu"
date = 2023-10-02
[taxonomies]
tags = ["projects", "webdev", "icc"]
+++

<!--
this article is really dry. i think its because i basically go
step-by-step through my process, and its just a "this happened, then
this happened, so i did this, then this happened" sort of process with
no narrative through-line. is it even worth writing such an article?
maybe shorten it to its minimal length, and just include it in the
monthly log.
-->

Web development has historically disgusted me.
So many frameworks, so much cruft spread across various W3C standards,
so much to know about, and an unappealing problem space.
That, combined with cultural elements of corporate minimalism, the rise
of Elektron-based webapps, and the infamous "move fast, break things"
approach have kept me mostly in the realm of desktop and embedded
software.
Which isn't to say I've never made a website. In fact, some of the
earliest software I've ever made were websites (the *actual* earliest
are probably games, but the timeline is hazy and the definition can
sometimes be blurry).

One of the projects that's been on my frontburner recently is
[incremental converse crowdfunding](/icc).
This is one of the few projects I've worked on that practically
*requires* a dynamic website.
I considered implementing it with email, or ActivityPub, or a TUI that
connects to a REST backend, but while none of those would require a
web frontend, I think it would be a challenge to make them comfortable
for a non-technical audience to use, and since I have to use web
technologies anyway, might as well just make a regular website.

I've been putting off actually working on it, out of a fear
of certificate authorities, nginx, DDOS attacks, authentication
(especially password storage), and handling money, just to name the
big ones.
But a friend recently made a website where he plans to sell software
directly to his customers (rather than through Gumroad, which is what he
currently uses).
Software politics aside, it was interesting to me that he was doing this
because it seemed pretty slick, and he seemed to lack the fear that I
had with my own website.
I assumed this was based on the fact that he was using a bunch of
existing services and frameworks, and the part of his stack that caught
my attention was Supabase.
I eventually realized that it didn't actually solve most of the more
prominent issues I was worried about, but at the time, the particular
features of Supabase that seemed appealing to me were that it handled
authentication, had a free tier, and was self-hostable.

Currently, I use [Rocket](https://rocket.rs) for my entire stack.
I initially started with [Yew](https://yew.rs), before realizing that
it's intended for frontends.
I might return to it later if I figure out that the frontend is
actually something where I need to have a lot of dynamic stuff going
on, because under no condition am I going to write any JavaScript or
TypeScript.

Without a cloud service like Supabase, my options are pretty
limited.
Either I can roll my own authentication (almost certainly a bad idea,
if years of pwns are any indicator), or I can hook into an existing
website's authentication, through something like OAuth.
One such standard that came up in my research was
[IndieAuth](https://indieauth.net/), which, as I understand it, runs
on top of OAuth to log people into sites using their logins from, for
example, their social media accounts.
This is an improvement from OpenID Connect, which requires each website
to explicitly allow logins from each provider it chooses to support.
Twitter used to support it, but after the API closures, it no longer
works.
There are some smaller tools listed on
the IndieAuth website, including a plugin for
[WordPress](https://indieweb.org/Wordpress_IndieAuth_Plugin), but the
biggest IndieAuth provider is probably GitHub.
I think moving forward, Fediverse projects will probably start to
support it, but right now, support for IndieAuth is pretty sparse.
That being said, it seems to be my best option at the moment.

*to be continued*

<!--
* indieauth
	* https://indieauth.com/
	* https://aaronparecki.com/2021/04/13/26/indieauth
	* https://indieweb.org/IndieAuth
	* https://aaronparecki.com/2018/07/07/7/oauth-for-the-open-web
	* https://brainbaking.com/post/2021/03/the-indieweb-mixed-bag/
	* https://docs.rs/indieweb/latest/indieweb/standards/indieauth/index.html
* databases
* using the ko-fi api
-->
