+++
title = "ICC Concerns"
slug = "icc-concerns"
date = 2023-09-25
[taxonomies]
tags = ["thoughts", "economics", "icc"]
+++

(This was originally part of the main ICC blog post, but I split it off
because it got too long)

Read this first: [ICC](../icc)

# funding comes after publication

For many products,
there is a large up-front cost required to even begin work on completing it.
This is true for almost all physical products,
especially tech hardware.
ICC has no solution for these cases.
It instead focuses on information-based products,
like art and music and software,
as they can usually be broken down into sub-tasks,
their distribution costs are almost zero,
and the cost to the author to get started is generally very low.

The risk has to go somewhere,
whether it is the funder risking the loss of their investment
or the producer risking the loss of the work they put in.
The reason ICC chooses for the money has to come after is because
it is more fluid <!--TODO: should I use the word "liquidity" here?-->
(i.e. you can't release 90% of an artwork,
but you can provide 90% of the funding),
and that fluidity is important to the scoring process.
Also, since there are generally far more funders than producers
in any given transaction,
it makes more sense to treat the funders
as a more consistent collective decision-making body
than the producers.

This is all to say that ICC isn't a viable means of funding for someone
with very little starting funds, so this will likely be used as either
as a side gig to complement a separate, more stable income, or for
artists who already have a significant fanbase.
However, no other method of funding really allows artists to start from
zero financial or social resources without putting all the risk on the
funder.


# ziglang bounty post

(added 23-268)

<https://ziglang.org/news/bounties-damage-open-source-projects/>

This blog post highlights many issues with bounties that might also
apply to ICC:

## Bounties foster competition at the expense of cooperation.

Possibly also applies to ICC. It depends on how an organization decides
to distribute a pledge that multiple people worked on, and ICC makes
few moves, if any, to help with this. The case that I see being the
most common is that donation payouts are shared, and new contributors
get a reduced share (the rest goes to maintainer) until they reach some
threshold. This has many downsides:
* for maximum payout, a contributor is encouraged work on a task alone
* new contributors are considered outsiders with reduced entitlements
* without good management, there might be conflicts when multiple
members work on the same task

In the case of an organization with multiple members, perhaps it makes
more sense for all donations to a project to go to one big pool, and
for there to be some other decision-making process for deciding how the
donations are distributed.

## battle royale dynamics

> Instead of scouting for a suitable candidate, you’re letting battle
> royale dynamics pick a winner for you, at the expense of everybody
> who’s going to lose the competition.

This observation highlights the importance of tying the pledge not
just to a work, but also to the artist. If a different artist produces
interchangably similar work, it is hopefully obvious that they should
not be entitled to all of the pledges that were made towards this
artist's work.

Likewise, by reducing or eliminating the payout in the case that an
outsider completes a task, these battle royale dynamics do not form, and
a suitable candidate can still be carefully selected.

## risk allocation

> Instead of creating a clear contract where you take on some of the
> risk, you implicitly put the entirety of the risk on the contestants
> (eg partial solutions don’t get any payout).

A later point that I believe is also related:

> Instead of spreading unease to all the people involved, it would be
> preferable you instead learned how to do business properly.

The allocation of risk to contestants only forms when the criteria for
payout is simply completing the task. As long as the payout is tied to
both the work *and* the artist, ICC does not allocate risk any more
poorly than it already is. That is, because payouts don't go to drive-by
contributors, the risk of two people working on the same task is the
same as it is in any other FOSS project since there is no financial
incentive for people not associated with the organization (assuming the
organization does a good job allocating work, which, to be fair, is not
a given).

## reckless action to pass a test suite

> Instead of allocating time and resources to proper due diligence, you
> instead penalize any form of thoughtfulness in favor of reckless action
> (eg a solution just needs to pass a test suite).

As far as I can tell, this is less relevant due to converse
crowdfunding. Thoughtfulness is still required because the final arbiter
for quality is the donor, who can use whatever subjective mechanisms are
necessary to assess quality.

## software lifecycle considerations

> Instead of planning for the full lifecycle of software, which also
> includes maintenance, you end up with a quickly bitrotting artifact
> that is of no practical use to anybody.

Funding maintenance is definitely a concern, especially for larger projects.
ICC does not work well in cases where a few very wealthy organizations
all want the same thing,
and most open source libraries fall under this case.
Maintenance is incredibly important (I think most developers would say
that it's even more important than new features), but very boring (both
for maintainer and user) and hard to fund.

The Zig post goes one step further and highlights that if a contributor
cares more about the payout than the long-term sustainability of the
project, then their priorities discourage them from submitting
maintainable code.

For individual artists, this seems like an easy, though not trivial,
obstacle to overcome. My worries are greater when it comes to larger
organizations and more complex projects (like Zig). I've been simmering
on an organizational structure that can work in tandem with ICC, but I'm
skeptical as to whether it will actually work:

All donations go to the organization, and is distributed based on
"shares", which are received when one completes a task. As a result, the
contributor gets much less up front, but over time, they get a portion
of all future donations. This encourages contributors to make their
contributions more sustainable over the long term.

While this seems better than simply giving all the donations to
the contributor that completes the task, I am a little hesitant to
prescribe such a bureaucracy, and I think this is something that should
be figured out by the organization in question.

## social pressure

> On projects less radical than Zig, you might also put pressure on the
> development team to accept the winning submission, which, given the
> above, will probably not be the most well-thought-out and maintainable
> solution.

I have no solution, except crossing my fingers and hoping people are
reasonable and understanding about this.

## conclusion

As I understand it, the conflict in the WASIX issue linked in the Zig
blog post mainly stemmed from the fact that implementing WASIX would
not have aligned with the goals of Zig.
Even though the bounty payont is not contingent on the implementation
being accepted into the mainline branch, it is understandable how this
bounty could be used as a tool to subvert the goals of the leadership
and community to push the progress of the project in a direction
that its participants don't want it to go in ([another relevant
section](#more-money-means-more-influence)).
While this concern is not explicitly mentioned in the Zig blog post, it
highlights yet another important aspect of ICC: the artist is the sole
entity that is allowed to create new proposals.
Before, I had made this decision because I thought it would be annoying
and unwieldy for the donors to be able to create new proposals (not to
mention the logistical headache of determining when two proposals are
similar enough to combine into one).
However, as this conflict shows, it is absolutely imperative for
proposal creation and modification to be restricted to the artist in
order to preserve artistic freedom, and it is perhaps worth looking
into additional protections.

Most of these points seem extremely pertinent to ICC (especially the
last two), and are worth thorough analysis during and after the initial
test run. It should not be understated the degree to which these issues
could result in harming FOSS more than it helps. The worst outcome for
this is that it leads to the destruction of a FOSS project that would
have otherwise survived, without preserving a project that would have
otherwise died.


# artistic freedom

I personally believe that this method allows for more artistic freedom
than copyright-based compensation schemes (like commissions). I have
no real basis for believing this, and will be on the lookout for the
effects of this funding method on artistic freedom when I see more
data.

In the end, the producer still gets final say in what they choose to do.
Even if one option has significantly more fund offers than the others,
the artist is still justified in deciding to do one of the others,
and those who offered to pay for the most popular one
don't lose any money from this.

# lack of consistency

One of the proposed benefits of ICC is that since publication is much
more incremental and permissive licensing is (theoretically) encouraged,
it is much easier for an unaffiliated group to continue from where an
abandoned project was left off.
However, with this scenario,
there is potential for an inconsistent final product.
For example, a project that seeks to adapt a comic
into an animated cartoon
may only end up creating 15 episodes
before the team decides to halt production.
In the case, another team could continue production,
perhaps with a different approach that makes it more financially efficient.
Whether this continuation is higher or lower in quality,
the collected works as a whole are slightly lower in value due to the inconsitency.
It becomes noticable after episode 15 that something is different,
and that breaks immersion for the viewer.

However, the alternative is for the cartoon to be completely cancelled,
so I don't see this as being any worse that the current status quo.

# more money means more influence

In general, people with more money have more power, and if implemented
without careful intent, this tool will likely cause the same power
consolidation effects of many other market systems.
One way to counteract this is some sort of voting system where each
person gets a single vote, but that has its own complications in
verifying that all of the voters are actually distinct people (and not
bots or malicious actors), or the fact that some people are more
reliant on a project than others.
That being said, the goal with ICC is to make money flow to people
making cool things, with no regard for power, and the currently
targeted demographic is one that is very conscious of how money and
power flow.
I've seen very few people who pursue art or FOSS software out of a
desire for money and power.
This is also why my platform (as is currently planned) requires FOSS
and/or Creative Commons licensing, as it filters out the people for
whom this is exclusively a means of making more money (which isnt to
say that making money can't be someone's primary goal).

Another potential concern is that
an individual with a large amount of money
could basically have monopoly-level artistic influence
over a large part of digital art production.
However, if this platform led to
a small amount of people paying artists large amounts of money
to produce what they (both the funder and the producer) want,
I'd say thats a perfectly good outcome.

# low variance and regular output producers

For a producer who only produces one type of content
at a fixed interval,
there isn't much economic benefit to incremental converse crowdfunding
over methods like crowdfunded patronage or donation.

## case study: 99% Invisible

The main work that 99% Invisible produces is their weekly podcast
discussing design.
There isn't really any reason for a listener to pledge towards a
"Release a podcast" product offering, as the quality, quantity, and
timing of that deliverable would be practically the same regardless of
funding amount.
Moreover, part of the joy of the podcast is revealing a part of the
built world that is often underappreciated, so holding a poll to decide
on the topic of the next podcast would be antithetical to this premise.
For this type of creator, it would make more sense to run funding
drives (which, last time I checked, is what 99% Invisible does), use a
platform like Ko-fi or Patreon, or seek public funding.

# research

(added 23-104)

It goes without saying that
research is an important part of the development process.
I believe ICC would not be ideal for directly funding research
for a few reasons:
* The value of research is hard to know before it is done.
Using exploration of a forest as an analogy, the biggest and most
important breakthroughs aren't those that cover the largest area,
but rather those in the path that connect the starting point to the
destination.
* It is hard to break down into smaller increments, as the value is
really only realized in aggregate.
A deliverable like "produce a reliable method to sense the position
of a shuttle along a rail" would have so many factors involved in its
assessment that even with conditional offerings, it would be difficult
to express the suitability across the various use cases, and how the
final solution navigates the many tradeoffs involved.

# accessibility

(added 23-268)

Accessibility improvements only really gets done via central planning
measures (at a small scale, Apple, at larger scales, the American
Disability Act).
I don't forsee ICC hurting current accessibility efforts, but it is
important to recognize that this is not really a "rising tide that
lifts all boats" in FOSS and free culture, and that other measures are
necessary.

# getting scooped

If you're a video essayist, you might want to create proposals that
describe the topic of the proposed video in detail.
However, doing so could result in a lower-effort essayist "scooping"
your topic before you.
While it would be interesting to see how the essayist would combat that
(perhaps by using the "scoop" as another reference to fact check their
own video, or to gauge the audience reaction and use that to expand on
some parts and remove others),
in general, the best move is probably to use more vague descriptions of
the topics in their proposals.

# a bit complicated

One of the claimed benefits
of Ko-fi over other platforms is its simplicity.
Once you incorporate details
like sub-deliverables and funder reliability index and conditional offerings,
ICC gets pretty complicated.
In addition, the onus is on the funder
to determine to what extent the producer has satisfied their claims,
which is additional work
that not a lot of supporters are willing to put the time and energy into.

Notably, the game engine [Bevy
observed](https://bevyengine.org/news/bevys-third-birthday/#funding-bevy-is-confusing)
that when it moved from a simple system (send all money to the
creator/maintainer) to a more complex system (choose which contributer
to send money to), **new donors went way down**.

# money corrupts the joy of creation

In some cases where there is no financial incentive, there's a lot
more passion, in part out of necessity.
But such a scenario is almost never financially sustainable.

See also: <https://queer.party/@Lyude/110861481994039644>

# CC-BY-NC

Not really an issue with the actual mechanism behind ICC, but it's
probably not legal for an artist to use this as a platform for a work
that triggers the non-commercial restrictions of a Creative Commons
license. While ICC is a framework for donation (it does not transfer
any rights or meaningful property in exchange for the donation), I
belive the non-commercial restriction still triggers even in the case
of donation-funded media (like Blender open movies). Just something
that it's probably worth clarifying to the artist.

# this seems pretty consumerist

Frankly, I believe markets, when used correctly, are a very useful
economic tool.
In particular, I appreciate how effectively they distribute the most
resources to causes that can provide the most direct utility with such
a simple system.
It does have its flaws, and is often misused (looking at you, Chicago
school of economics), but I think that the general idea of designing a
market-like space that encourages commence is a good way to get things
made.
Sure, this does "encourage the acquisition of goods and services in
ever-increasing amounts" (which is how Wikipedia defines consumerism),
but, for better or for worse, that's just the nature of money and
fundraising.
If you're looking for a less consumerist solution, here are some
options:

* lobby politicians to increase funding for public services like radio
and television
* publicly voice support for your country's endowments and fellowships
(like the National Endowment for the Arts)
* support the artists in your local and online communities
* go commission a profile picture or something. artists are really good
at that kind of thing, and its really satisfying to show off something
that was made just for you. also maybe like a ringtone.
	* <https://banchan.art/>
<!--* [Francis Muguet's Mécénat Global](https://www.itu.int/net/wsis/implementation/2009/forum/geneva/Pdfs/WSISForum09-patronage-V0%202.pdf)-->
* <https://comradery.co/>
* <https://resonate.coop/pricing/>
* Benn Jordan's proposal of [socialized copyright](https://youtu.be/PJSTFzhs1O4?t=509)

# closing statement

For the foreseeable future, any attempt to fund a cool public project
is a balance of doing the right thing for the future of the project and
finding a sustainable source of income.
If improvements came without costs and sacrifices, then things would be
much easier.
Hopefully, this paradigm is an improvement on the options currently
available, but that remains to be seen.
