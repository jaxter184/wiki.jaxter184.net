+++
title = "ICC Prior Art"
slug = "icc-prior-art"
date = 2022-12-28
[taxonomies]
tags = ["thoughts", "economics", "icc"]
+++

Continued from: [ICC](../icc)

# bounties

Bountysource is a platform that allows bounties to be assigned
to feature requests in an open-source code repository
like GitHub
(it currently does not support sr.ht, GitLab, Codeberg, or Bitbucket).
Bountysource differs from incremental converse crowdfunding by:
* allowing both users and developers to make bounties for a project,
rather than just the project owners
* acting as an escrow between feature requesters and devlopers
(ICC explicitly does not hold any money,
and allows funders to withhold funding after the deliverable is published)

Other examples:
* <https://github.com/obsproject/obs-studio/wiki/OBS-Project-Bounty-Program>
* <https://contest.com/>

There have recently been quite a few arguments against using bounties:
* <https://twitter.com/mitsuhiko/status/1703452194429690346>
* <https://ziglang.org/news/bounties-damage-open-source-projects/>

A point-by-point analysis of the ziglang blog post is available [in this
supplementary reading](../icc-concerns#ziglang-bounty-post)

# patronage

I am personally a fan of the feudal patron system
where individuals with large amounts of money (like the Medicis)
fund artists (like Mozart or DaVinci) to produce art.
While this doesn't really make sense in the modern world
where we have the labor specialization to fill important jobs
like food production efficiently with just a few people,
leaving pretty much everyone else to do whatever they want,
and wealth consolidation is generally frowned upon,
given the situation,
I think it's cool that rich people (and churches)
had an appreciation for the value and utility of the fine arts.
Rich people these days have no sense of noblesse oblige.
That being said, it is neither democratic nor sustainable
for the power of patronage to be so densely concentrated
to so few people.

# crowdfunded patronage

To make the system of patronage more democratic,
Ko-fi and Patreon take this model and combine it with crowdfunding.
Both these platforms are a pretty good way for producers
with a decent following
to get money from the people who consume their output.
In general, I am in support of these platforms (Ko-fi in particular),
and I believe these types of crowdfunding will still have a place in the future,
but a qualm I have with them is the issue of cross-subsidization.

To focus on a particular example,
let's look at the Patreon of insaneintherainmusic,
who makes Jazz (and Jazz-adjacent) covers of video game music.
Funders pay a set price per cover video,
which suggests that that is the product they are paying for.
However, the patron tiers tell a different story;
depending on how much a funder pays per video,
they receive perks like
access to special patron-only communication channels,
early previews,
and free lossless music downloads.
In terms of incentive,
it is clear to me that these perks,
and not the actual videos,
are what these patrons are paying for.
After all, the videos will come out regardless of whether
I, as an individual, choose to give $5, $1, or $0.
For producers who don't have this kind of tier,
Patreon is essentially a donation platform.

Ko-fi is similar, but only allows monthly videos
(rather than per-deliverable),
and also has infrastructure for
shops,
commissions,
and one-time donations.
While their language is more clear about the fact that you're paying for the perks,
some users don't have perks,
or the perks are very low in value
compared to the value of their creative or technical output,
which, again, means that this is essentially a donation platform
(but a very successful one at that).

# case study: Philosophy Tube reading list

Philosophy Tube is a YouTube channel that aims to "give out a
philosophy degree for free" (paraphrased). There used to be a book wishlist
where people could buy books for the channel, and it is explicitly
advertised that buying books from this list could have a big influence on
the topic of future videos. When I first found out about this, I was
taken aback by the striking similarity this has to ICC. In both cases,
the artist proposes topics for future works, and people can use
donations to steer the direction of the channel. There are a few critical
differences, however:
* there is no guarantee that an episode will be published that uses
the book
* funding is one-to-one; rather than being selected by a crowd, it is
selected by an individual
* it does not scale very well (I'm sure Abigail reads books very
quickly, but I'm also sure there is a limit to the speed at which she
can read)

I believe that this wishlist has been abandoned, and the channel is
mainly funded through Nebula and Patreon, which is understandable given
the last bullet point and the popularity of the channel.

# commissions

It's great that technology has made it so easy for me to commission
a work of art from an artist I like. However, commissions are pretty
expensive, and are generally heavily restricted by copyright. This
leads to a less extreme version of the issue with patronage, where
only people who can afford high prices for a fairly useless product can
influence the creative output of artists. In addition, this is a pretty
inefficient use of artist time; since a digital product can be copied
an arbitrarily high number of times, there's a lot of missed revenue
from only being able to sell the product to one person.

Commissions can be done fairly easily even without a dedicated
platform, but Fiverr is a site that specializes in commissions. I've
used Fiverr in the past, and it's fine, but it's not really
producer-focused, and the quality is a bit shoddy.

As a sidenote, it may be useful going forward to think of ICC as a
combination of commissions and crowdfunded patronage.

# donation

Donation is a great way for a funder to show their appreciation for
the producer in a tangible and beneficial way. However, the thing that
makes this method of funding so meaningful, namely the lack of direct
incentive for the funder, is exactly what makes it an unreliable
and disproportionate form of funding. Unreliable because whether a
person does or doesn't donate is difficult to predict and influence,
and disproportionate because better quality or quantity doesn't have
a direct influence on the donation total.

OpenCollective is a donation platform with a focus on transparent
spending. LiberaPay is a donation platform where funders can provide
periodic donations to producers who contribute to the commons, whether
they "make free art, spread free knowledge, [or] write free software".

# advertising/sponsorships

I'm not a fan of advertising.

I don't like that I, as a third party, can get a producer to leverage
the trust they have with their audience to make me money. If I don't
advertise and my competitors do, and my product is not good enough to
compensate for the difference in brand recognition, then I lose sales.
I'm not sure what the ideal method of informing consumers is, but I
think the current state of advertising is definitely not it.

I don't like that I, as a producer, have to choose between ethics
and money. Sure, I have the option to just ignore sponsors if I value
ethics over money, but say I were a 3D printing video producer, and
other people in my space are getting paid to shill some 3D printer
filament while I have to pay for my own, plus they can use that extra
funding to make their product better. Similarly to the previous case,
I'm at a disadvantage as a result of my stubbornness.

I don't like that I, as a consumer, can be so easily influenced by a
third party to buy their product. This ability to influence me also
gives them a financial incentive to gather data on my behavior, ideally
without me knowing. Knowledge is power, and through advertising, that
power can be leveraged to move money from my hands to theirs. Also I
think advertisements are annoying and I have yet to hear of a single
person who believes otherwise.

To be clear, "advertising sponsorships" refers to a third party giving
a producer money in exchange for promotion of the third-party's good
or service to the producer's audience. Affiliate links are often used
to track the effectiveness of these sponsorships. Systems like GitHub
Sponsors are more akin to crowdfunded patronage or donation.

There are some benefits to advertising though, and these benefits make
it a very enticing (and difficult to replace) system for the people that
use it.
The main benefit (for the advertiser) is that participation is
extremely passive.
Advertising requires no login, no payment information, no calls
to action.
Sometimes can even happen without the viewer noticing with techniques
like product placement (and that lack of awareness is, in my opinion,
a big part of the problem).

# microtransactions

In a sense, it is the opposite of advertising, in that participation is
much more deliberate, and rather than targeting a wide range of people,
it targets a very small subset of people who pay large amounts, and
constantly re-affirm their committment.

I also put things like [TikTok's live
gifts](https://www.tiktok.com/creators/creator-portal/en-us/getting-paid-to-create/live-gifting/)
and Twitch bits in this category, but they are a notably distinct subset
that encourages its own behaviors.

# non-fungible tokens

While as far as I can tell, they were a passing fad that pretty
much disappeared as soon as they started being regulated, NFTs were
advertised as a solution for this type of problem, and I think this
type of funding mechanism is worth mentioning. Non-fungible tokens
were a means of abstracting a concept or non-tangible item into a
tradeable token, allowing the person who was associated with the token
to trade it with others as they would a unique physical item. However,
for the purposes of comparing NFTs to incremental crowdfunding, we can
also consider physical "tokens" like limited-edition vinyl releases or
signed items. The issue I have with all of these products is the fact
that they only provide funds by cross-subsidizing other offerings. With
this useless token whose value is practically only influenced by how
much other people are willing to pay for it, you can take a token
like a vinyl record that costs $6 to produce, and sell it for $100
(or more). Its value comes primarily from its rarity, which limits
the amount of these that you can produce before you start losing
money, but for most people, it's worth making a few of these tokens to
cross-subsidize their actual product, even if the collectors make far
more money than the producers.
