+++
title = "ICC Continued"
slug = "icc-continued"
date = 2022-12-28
aliases = ["icc-continued"]
[taxonomies]
tags = ["thoughts", "economics", "icc"]
+++

Read this first: [ICC](../icc)

# Motivating theory

The base concept motivating the creation of ICC is that data alone has
almost no value. It can be trivially copied and distributed at almost
no cost. Where the value comes from is the fact that someone organized
it, whether by writing, debugging, maintaining, sketching, inking,
coloring, modeling, posing, animating, designing, etc. The work comes
not in generating the bits that define the file on your computer, but
rather by making the decisions that refine those bits into something
meaningful.

<!--
https://viralinstruction.com/posts/defense/

Software is not unlimited

As an artifact, software is quite different from the physical creations
of other crafts: Producing it consumes no raw materials. It requires no
specialized tools to manufacture even the highest quality code. The
product has no weight, and its physical distribution is almost
effortless. There is almost no cost to producing millions of copies and
shipping them all around the world.

So, without these constrains, is software unbounded, infinite? No, it
is held back by other constrains. Sometimes software is constrained by
the physical capability of the machines it runs on, disk space, memory
usage or speed of computation. I don't want to discount these physical
constrains: After all, much of what I've written on this blog is about
performance. But mostly, software is bounded by its creation process:
Programmers have limited time to create, and especially limited time to
maintain, code.
-->


# Donor Reliability Coefficient

<!-- casual version:

One potential formula for this metric is $$I_0 = P/O$$, where
$$I_0$$ is the index, $$P$$ is the total amount a user has paid,
and $$O$$ is the total amount a user has offered. But alas! If the
fraudulent funder has previously made a full payment of an offer of $1,
then they have a maximum reliability index, and we're back to square
one.

To fix this, let's scale the index by the amount paid: $$I_1 =
P*P/O = P^2/O$$. Now, while the fraudulent funder still
has an index of 1, a genuine funder who has paid $120 of their proposed
$200 over the past 2 years has an index of 72, greatly outshining the
fraudulent funder.

-->

In my opinion, the most obvious concern is: if a donor can withhold
their donation, what's preventing them from making a very large pledge
and not paying after the work is published?

My proposed solution to this is to assign each donor a "reliability
coefficient".
This metric would roughly predict how likely it is that a given donor
will fulfill their pledge based on their past donations.

Lets consider an example:
A fraudulent donor made a large pledge for proposal `A`
that they don't intend to pay.
I, as an artist, am choosing what to work on, and I see that
proposal `A` has a much higher payout estimate than proposal
`B`, and work on `A` as a result.
Once I release my work, I end up not getting paid nearly as much as I
would have for `B`, which, in this example, has mostly genuine pledges
(though I cannot know that at this point in time).

This is where the "donor reliability coefficient" comes in.

One potential formula for this metric is
$${C_0 = \frac{D}{P}}$$
where ${C_0}$ is the coefficient,
${D}$ is the sum of a donor's past donations,
and ${P}$ is the sum of a donor's past pledges.
But alas!
If the fraudulent funder has previously fulfilled their pledge with a
donation of $0.01,
then they have a maximum reliability coefficient,
and we're back to square one.

To fix this, let's scale the coefficient by the amount paid:
$${C_1 = P\frac{D}{P} = \frac{D^2}{P}}$$
Now, while the fraudulent funder still has an coefficient of 1,
a genuine funder who has paid $120 of their proposed $200 over the past 2 years
has a coefficient of 72,
greatly outshining the fraudulent funder.
72 is a really big number though,
and over the course of many years,
it could be reasonable for a third donor to spend thousands of dollars total,
making their "influence" many orders of magnitude higher
than even the donor who has spent $120.

Let's try adding a time scaling factor.
$${C_2 = \sum_i e^{-t_i} \frac{D_i^2}{P_i}}$$
where each payment/offering pair is scaled by how recent it was,
${t_i}$ being the amount of time that has passed
since payment ${i}$ in months (an arbitrary time unit).
Doing some napkin math,
it seems like this helps with balancing
the influence of long-term donors vs medium-term donors.
We could add a few scaling and offset factors here and there,
but for the most part,
this seems to solve the problem at hand.

It's worth noting that this is definitely not the only solution to the
stated problems, and almost certainly not the best! Perhaps it would be
worth allowing artists to mess with these formulae and coefficients in
order to let them use whatever they believe best predicts payouts.

One last potential issue I want to address is how fund offering amounts
scale the coefficient.
Someone who has paid $120 over the course of a year
suddenly declaring a $2000 fund offering is a bit suspicious,
so we should incorporate that into our final calculation somehow.
However, I think it's fine to leave the coefficient as it is for now,
and figure that out later if it actually becomes a problem.

As it stands now,
the donor reliability coefficient roughly maps
to the amount of money a donor is likely to provide
based on all of their past donations.

The calulation of this coefficient is critical to the functionality
of converse crowdfunding, as it is what allows ICC to still work even
though donors are allowed to retract their entire pledge after the work
is published.

## sidenote

As an exercise for the reader,
consider the following:
a fraudulent artist makes a proposal
that they do not plan on fulfilling,
and some donors declare pledges.
In this example,
the artist fraudulently "publishes"
a trivial or low-quality work
(say, a blank white image file rather than a page of a comic)
just to claim the pledges.
How can the system be built to minimally punish these donors?
It's easy to say that the donors should have been smarter,
and should either pay the amount they offered
or retract their entire pledge
and just take the hit to their reliability coefficient,
but ideally,
the system should try its best to nullify any incentive
that would make the donors want to pay the fraudulent artist.
In the long run,
this type of fraudulent artist wouldn't last long,
but one could easily imagine a washed-up one-hit-wonder artist
making one last grand proposal,
to which many donors offer pledges,
just to run into the above situation
where the work is low-quality or nonexistent,
intended only to trigger the donation invoice.
<!--
In this case,
perhaps we could normalize the offerings?
If almost every funder withholds
part or all of their compensation,
we should be able to use that
to quantitatively measure
the extent to which the producer "failed" to deliver.
-->

Could the donor reliability coefficient be adjusted to mitigate these
scenarios? Would it require a totally separate mechanism?


# conditional offering

In some cases,
it may make sense to offer additional funds
if some conditions are met.
For example,
for a chapter of a comic,
there could be bonuses
for translation to a different language,
or for some color panels.


# sub-deliverables

A primary component of incremental converse crowdfunding is for
a product offering to be split up into as many smaller parts as
reasonably possible.
In order to simplify pledging, it should also be possible to donate
to a higher-level task, in which case its sub-tasks should trigger
a partial donation for the pledge, perhaps weighted based on the
significance of each sub-task.


# social engineering

## acknowledgement

There should be some way to prove that you helped make a feature
happen. Maybe also a percentile metric comparing you to other donors.

In my observations, being recognized for donating isn't really
something that individuals tend to care about.
Usually, it's corporate sponsors who care about brand visibility.
As a result, its unlikely that a "gold level sponsor" donation badge
would even be an incentive.
Instead, maybe badges should be linked to specific updates or
features?
Being recognized for their contribution in *driving the direction* of
the project seems like something that a donor might take more pride in.

<!--
## peer pressure

if you want a feature, then you can just offer funds. make people feel
like they are actually being heard and making a difference
-->
