+++
title = "Tone Tram"
slug = "tone-tram"
date = 2023-10-02
[taxonomies]
tags = ["project", "hardware", "music"]
+++

[aliqot](/prj/aliqot) was a project I did in my undergraduate studies to
build a digital music controller abstractly based on the bass guitar.
The main sensing mechanism was a long row of capacitive touch sensors,
intended to sense where along the neck the player was touching, and how
much of their finger was touching the surface.
In the end, this barely worked, and was a bit clunky to use, and I
therefore didn't end up building a full-scale version of the device.

Fast forward several years, and for some reason, I have the sudden urge
to revisit this idea. But first:
* Someone who made something cooler and more useful and more concretely
similar to a bass: <https://youtu.be/4m1YorzgxkY>
* A commercially available device that (I think) uses the same sensing
technology: <https://maywadenki-os.stores.jp/items/5eef5b521829cd47b90668be>

# de Bruijn sequence

The main reason that I came back to this concept is because on a
complete whim, I wanted to know whether there was a shift equivalent to
the [Gray code![wikipedia][wl]][grac].
That is, in the same way that the Gray code could be used to encode an
absolute position in a series of values that progress by flipping a bit
in one position, was there an equivalent code that let you do the same,
but using bit shifts rather than bit flips?
The main benefit of such an encoding is that instead of using N
channels of bits, I could just use one channel, which saves a lot of
money in magnets and a lot of labor in putting things together, plus
its much easier to design and produce around one row than N rows.

<!-- TODO: diagram -->

It didn't take a lot of searching to come across the [de Bruijn
sequence![wikipedia][wl]][debru].
It seems applying this concept to sensing is not an uncommon practice,
but much of the literature discussing it is paywalled.

[grac]: https://en.wikipedia.org/wiki/Gray_code
[debru]: https://en.wikipedia.org/wiki/De_Bruijn_sequence

# construction

TODO
* hall effect sensors
* gluing magnets is a pain
* modular connector that works with my cowbell pcb


[wl]: /images/wikipedia.svg
