+++
paginate_by = 12
sort_by = "date"

extra.after_dark_menu = [
    {url = "$BASE_URL/deu", name = "Heimat"},
    {url = "$BASE_URL/deu/tags", name = "Tagen"},
]
+++

# blog - jaxter184 - um Musikentwickler-Generalisten zu werden
