Explore what different sound terms mean, both connotatively and denotatively. Understand what the human relationships with those sounds are, as well as why they are popular or why they are used.

What is the place of technology in music? While it has certainly expanded the possibilities, it has also, in a way, sterilized it. The best musical technologies have been very easy to interact with, though not necessarily incredibly intuitive.

## what does computer music performance have that regular music performance doesnt?
- Perfection
	* "imperfection is the digital perfection"
	* computers can be so much more precise than people
- Memory
	* digital instruments can remember past inputs, allowing for state machines

## Bass

### What is bass?
* low frequency
* is the sound named after the instrument or is the instrument named after the sound
* bass baritone tenor alto
* DOUBLE BASS
* neuro bass
* core of electronic music
* Basis of music is drums and bass
	* thats all you need to make someone dance

### Brainstorming
* slap bass articulations
* long unitary string
	* there is no such thing as a bass instrument that has a wide expanse of keys
	* even bass synths are more compact than regular synths
* 4 finger buttons on left hand for root, fifth, seventh, and octave
	* maybe extra thumb button for repeated notes
	* maybe dial to adjust seventh
	* would make it centered around western music theory
* two "strings"? not really necessary since the one string is multitouch
* right hand has no articulations for now
	* I would like to make it more expressive, but my main priority is the tactile feedback, and i cant think of a good sensor that is both expressive and tactile
* bass in electronic music is distinctly different from bass in acoustic
  music

## Reverb

### What is reverb?
* sense of space
* resonance
* tells people what the space is like by comparing the sound they hear to other contexts in which they've heard it
* cathederals

### Brainstorming

* iris that changes size
* dull colors
* large instrument
* sound is based on surrounding environment
* one of the most literal cases of something sounding like how it looks

## Drums

### What are drums?
* very easy to play, incredibly intuitive
* tends to be very repetitive
* modern pop music has specific patterns

### Brainstorming
* how future-proof do I want it to be?
