# Adventures in mechanical CAD with Python

I haven't done much 3D CAD since I moved to Linux, and I blame that on the fact that Fusion360, which I used for a couple years up until recently, requires Wine to run. I've used FreeCAD in the past, but I've found that it's not very polished despite being very capable. I can get things done in it, but nothing feels like it's completely intuitive, so creating 3D models ends up feeling like more of a drag in FreeCAD than in Fusion360 despite being technically similar.

Now, usually, one would try to find a more intuitive CAD software that runs natively on Linux, or run it with Wine or on a Windows partition.  However, because I am a complete dunce, I have decided to try making all of my models in Python. My reasoning for this is that I found that the most useful part of Fusion360 is the parametric design aspect, where I can go back and change the amount of an operation or the dimension of a sketch. I assumed that it would be easier to learn a completely different interface that does a better job of than than to try and adjust my workflow to better operate on FreeCAD. Somehow, I was completely wrong in making that assumption, yet I'm very happy with the results, and will probably do all my mechanical CAD in Python going forward.

### FreeCAD

#### Installation

Because I need the `freecadcmd` environment, I can't use the appimage. (I read something about running `./freecad.appimage -c` to get headless FreeCAD, but I never got it to work). I got it working on my laptop, but because `pip` was being finicky, I couldn't get it working on my desktop. I think I'll revisit it again in the future, but for now, I'm just trying it out, so the laptop will be enough.

#### First impressions

After installing, I made (read: copied and modified) some basic scripts for creating STL files for an aluminum extrusion bracket.  Rather than using sketches and extrusions, I built everything with primitives and boolean operations, which isn't ideal, but it was the easiest way to get started.

Right off the bat, there were 3 primary issues:
* No modularity; things like `if __name__ == "__main__":` don't work
* Weirdly documented
* Issues with exporting curves as STL

The last two I could definitely overcome with some more time, but the first issue was a big showstopper for me. The way I like to organize my work requires lots of separate files for separate processes, and as much modularization as possible. While many of my issues with FreeCAD were small, they were indicative of a much more abstract conceptual disconnect between me and the software. I think I could learn to work with it after a while, and might even come back after trying other things, but for now, I'd rather try something more modularity-friendly.

If I'm going to be completely honest, I don't really have a legitimate reason to not use FreeCAD, whether it be the GUI or the headless Python scripts. There were a lot of little emotional hiccups, like the the fact that `freecadcmd` had a huge intro block. All of those little things added up to make me feel like the software was working against my wishes rather than guiding me towards the solution.
[!image](freecad intro block screenshot)

### python-occ

#### Installation

I used the aur packages (python-occ 0.18.1 and oce 0.18.3 at time of writing) to install python-occ, and it took forever. The first time I installed it, something went weird and I couldn't `import OCC` from Python. Not sure what the problem was, but uninstalling it and reinstalling it (almost an hour long process) ended up fixing the issue.  A classic case of 'try turning it off and on again'.

#### Documentation

The online-accessible documentation is pretty sparse for python-occ compared to FreeCAD, which has a wiki, forums, and various blog posts.  For python-occ, I found one blog post and two very very short documentation pages. The reference document that I found that was the most helpful is the python-occ-documentation GitHub repository, which I frankly should have used from the beginning. It makes you compile the html yourself using `sphinx`, but it's a pretty easy process. I was able to install `python-sphinx` using `pacman`, but I had to use `pip` to install `sphinx_rtd_theme`. I'm not sure what it does, but hopefully in the future I can use it to dark-theme the documentation.

#### Impressions of python-occ

Fun! I really enjoyed learning and using it, and every step of the process felt like I was working towards something meaningful. Everything I wanted to do felt like it was implemented in the way that it should have been.

## Footnote: Why not OpenSCAD?

I don't like functional programming, and I hear it's annoying to use without a wrapping layer on top. Also, from the examples I saw, I don't like how the code looked. Also, no options for STEP, which is pretty huge.
