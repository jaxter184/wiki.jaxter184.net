## HXCSTR

You can tell I was in high school when I made this because the name is
complete nonsense.

### The problem

This project came out of a personal wish to perform live electronic
music.
The primary product this is based on, the Launchpad Mk II, is a square
grid of 64 red/green LED backlit silicone dome keys.
While this was a suitable setup for some cases, I often found myself
needing more buttons, and could not figure out an intuitive way to play
it like a standard pitched instrument.
In addition, this time in my life was a pivotal point in my love for
hexagons.
In my research, I had discovered the sonome, a large (and expensive)
isomorphic keyboard.

### The solution

After seeing the viability of a hexagonally arranged set of keys,
I sought to make a keyboard that was inexpensive (below $500 per
unit, optimally below $200 at large scales), RBG LED backlit, and
user-reprogrammable.
Other than these necessary elements, other design priorities included
satisfying physical and visual response, ergonomics, and ease of
manufacturing.

The key offset spacing was selected to be similar to that of a piano
key.
Additional spacing between keys is not something that existed in the
Sonome, but I thought that it would be necessary in order to reduce
friction between keys.
It was not, and I definitely should not have included spacing between
the keys.

## Saturn

Modularity.
It provides expandability, flexibility, compartmentalization,
customization, resource efficiency, and much more.
Saturn was my first foray into modular music controllers.
It revolved around shift registers and analog gates, as well as the
assumption that LEDs would be the only output.

## Praxis

After the HXCSTR, I was looking to better understand how musicians use
various interfaces to create music.
The piano-style keyboard is a classic interface that has survived since
roughly the fourteenth century.
It seems to be the most common interface for contemporary electronic
musicians, even proliferating into live performance, as shown by
artists such as Haywyre and Porter Robinson.
Through the Praxis, I hoped to expand the possibilities of the keyboard
instrument through the Saturn infrastructure.

## aliqot

The successor to Saturn.
By creating a modular infrastructure for low-latency, multidirectional
communication of sensor and display data, I can easily produce
commissioned controllers in the future.

## Koppel

Though the survival of the keyboard instrument shows that it has a
place in the realm of electronic music, I believe it is far more common
than it should be, forming the basis of supposedly universal standards
such as MIDI.
Interfaces are an integral part of the workflow of any process,
expanding or restricting the creative and technical bounds within which
every user works.
By limiting interfaces to a mere keyboard, we limit the potential of
live music as a whole.
Though alternatives exist, they lack the diversity and proliferation
that the keyboard family of instruments provide.


# tangent: expression

with less expressive instruments, its harder to express the sounds that
we hear in our heads.
The nuance and versatility of the spoken word is what allows for it to
be used as a medium of communication.
While music is not exclusively used for communication, it's versatility
has allowed for it to be used as cultural glue.
By giving instruments more expression, we dig the roots of music
further into the soil of culture, allowing it to suck more elements
from the world around us and express them in a new and beautiful way.
