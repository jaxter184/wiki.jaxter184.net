---
title: Thoughts on the Rust programming language
date: 2021-10-16T03:57:42-04:00
taxonomies:
  tags: [thoughts, software, rust]
---

### What's Rust?

I haven't made too many posts on the Rust programming language
(or many posts in general), but Rust is a relatively new **systems
programming language**.
What does that mean? Who knows, I feel like people just make up these
terms.
To put it concisely, though, the goal of Rust is to be as fast as C
while not being as vulnerable to memory issues.
One of the big things that almost every introduction to Rust shows
is a graph of bugs grouped by cause, and whether it's a Microsoft
cross-company audit or a study on the Linux kernel, memory bugs make up
more than half.
Anyway, all of that is irrelevant to me because I barely know what a
"memory" is.

Fortunately, the Rust team thought ahead and sprinkled a ton of neat
features into the language to make it appealing to people like me.
I keep learning about cool new little things every time I do anything
in Rust, but of the cool features that I've discovered so far, the
ones that make it hardest for me to go back to using C are `cargo`, the
package manager, and algebraic enumerations.
There are other things like `serde` and rust macros that seem very
unique to Rust, but I think the best way to sum up why I like Rust
so much is that all the things that it has, it feels like they're
well-thought out.
The language as a whole feels very robust.
