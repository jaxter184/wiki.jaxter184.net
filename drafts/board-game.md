My high school friends and i started talking online more due to the
coronavirus quarantine, and one of the thing we mentioned wanting to
do together was play chess. As ive dug deeper into the world of chess,
the strategies and situations that arise are something that I dont see
a lot in the other strategy game that i've been playing recently, which
is Magic: The Gathering. The concept of deckbuilding as a game mechanic
is very fun in my experience, while still being something that can be
incredibly strategic. In my opinion, the best and worst thing about
magic is the randomness. On one hand, it makes the game exciting and
interesting, and keeps both players on their toes. On the other hand,
sometimes it can feel like victory was influenced more by luck than
by actual skill or strategy, which isn't fun for either the winner or
the loser. I have determined that the randomness mechanic brings more
negative aspects to the table than positive, and have decided to try to
make a game that retains the intrigue of a collectible card game while
getting rid of the influence of luck.

What does luck bring to a card game? Well for one, in a game like magic
where turn 1 win combos exist, it forces players to depend on a large
scale strategy rather than a specific card or set of cards. While the
decks can still depend on specific cards to come out, because they
do not come out on command, it creates a difference between the early
game, where you are holding out until you get the cards you need, and
the late game, where you are making careful decisions with large impact
based on the cards that are on the field and in your hand.
