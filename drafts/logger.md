---
layout: post
title:  "Logger"
permalink: logger
categories: jekyll project
---

At some point in my life, I decided that I wanted to keep track of the
time that I spent doing the various things in my life.
I wasn't sure exactly why or to what extent that I wanted to do it,
but I felt like it would be a cool and interesting thing to do with
my time.
The first attempt that I made at this type of logging system was to
make a spreadsheet that kept track of what I was doing at 30 minute
intervals.

Based on my initial entries, I think I wanted to keep track of what
projects I was working on and stuff.

### Moving

#### Reasoning

It feels kinda icky to keep this data on Google's servers, and if
something goes wrong with the security, I'd rather have it be my fault.
Also, Google Sheets is a total pain to edit in.
I find the autocomplete is way too aggressive, and it helps me about 1
in every 10 times it shows up.
Also, copy/pasting is super finicky, and im always on edge about
whether the smart formatting is being overwritten or something.
IFTTT is also pretty awful.
The GPS is pretty inaccurate the first time you use it after changing
locations (not a huge deal since I usually make at least two entries
per location, one when arriving and another when leaving) and for some
reason, the button widgets get a little finicky after restarting or
when on mobile data.
Also, the servers go down pretty often, but I think that'll happen
with my new setup too (though this is no longer important as I explain
later).

By making my own "app" for logging, I also have finer control over the
behavior.
The feature that I'm most excited about is being able to make entries
without internet access.
The difference in clock might be a little annoying, but I don't really
care about having millisecond-level precision anyway.
As long as I have GPS, ill be able to seamlessly make entries as if I
have internet.
This will also basically negate any issues caused by self-hosting since
server uptime is more or less optional.
When I was making entries without internet in the past, for example
during international travel, I would have to write down the time and
description manually in a notes app and enter them into the spreadsheet
by hand later when I had internet access.
In addition, I didn't have an easy way to get GPS data, so I had to
write down my location and get GPS coordinates later or omit location
altogether.

Termux GPS just works as long as you have a GPS signal.
It does take a little longer, sure, but I only have to wait once every
time I change location, and not every time I make an entry.
This is the best positive change to come out of this move that I didn't
predict or expect.

Most of the move was easy, just exporting the spreadsheet to .csv
format and writing a python script to convert it to the new format
(ISO8601|location|description).
I wrote a script in Termux to ssh and run a script with some stdin
info, and refactored my gui logic to work with the new format, and I
was pretty much set.
There were a few hiccups, one being the varying formats and syntaxes
that I used in the past to make this gui.
The biggest change was dropping the category number.
I feel like I can infer the category by the name of the command, and if
I can't, then I still have a syntax for explicitly stating the category
in the new format.
This saves me a lot of redundant information.
Another method for saving information is saving common locations, like
home and school and work.
Like with the category change, I still have a method for saving a
location in the row as gps coordinates, but I think the vast majority
of my locations are places that I will be frequently.

When converting to the new row format, `sd` was super useful.
In the initial conversion, I concatenated the category, description,
and notes fields into a single description field.
To make my gui pretty, though, I had to first get rid of the redundant
information.
`sd` is basically `sed`, but written in rust.
the syntax makes it a lot easier to use than `sed`, and it feels like a
real unix tool (do one thing, do it well).

### Conclusion

I hate the sun.
