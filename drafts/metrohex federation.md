## The problem

Have you seen the world nowadays? Everything seems like it's terrible. Emphasis on the word 'seems'. People are living really good lives, except for the fact that everyone is stressed all the time. Even when the world is great, everything is terrible.

This is really just an excuse for me to have fun learning a lot of different stuff that I usually wouldn't think that much about.

## The solution

The problems of the world aren't simple, and neither are the solutions. But there can be no good solution if there is no good discussion. My goal is not to create the solution, but rather to enable it. If a hypothetical country is created, then what qualities would make it the best?

## Standards

Dvorak. Lojban. Senary. So many of our standards are based on poor choices. We have the means and reasoning to make better choices now, and will probably have better means and better reasoning to make even better choices in the future. As times change, so should people and their standards. The fact that parts of the world are using standards more primitive than the metric system is deeply saddening. Humans are supposed to be the ultimate life form on Earth, and we have achieved such great things, but how is it that some of us still believe that pounds and ounces make sense as units of measurement? Sure, they were fine when we were trading grains and making wagons, but how have we been so averse to changing the standards upon which we build our civilizations that cars still use the same dimensions of chariots from back when years were still 3 digits long? At the very least, we as a society should be able to unianimously decide what we would use for these cases in the very best case scenario. We, of course, cannot be expected to migrate the entire basis of our engineering system every time we find a marginally better solution, but I would argue that we are far beyond that point for many of the world's systems. Not only that, but the rate at which we update our systems shows a deep flaw in society's ability to adapt.
