---
layout: post
title:  "on keyboards"
permalink: keyboards
categories: jekyll thought input keyboards
---

Keyboards have been around since the computer became a household item. They have remained largely the same since their inception. In fact, they have been pretty much the same since typewriters were a thing. There are a couple problems that I think the standard keyboard has.

## Ergonomics

Pronation and such.

## Thumbs

Thumbs are horribly underutilized on a keyboard. Their opposability makes them useful for modifier keys like shift and such, as well as stuff where you press keys repeatedly like arrow keys and tabs. However, on a standard keyboard, the only thing theyre used for is the spacebar, and the occasional `Alt` modifier.

## Key layout

QWERTY is fine. Dvorak isn't much better. But if you're going to use an ergonomic keyboard, I would highly recommend Dvorak. The different context makes it very easy to use, and I've been using QWERTY on my laptop and Dvorak on my desktop with negligible speed losses. I would like to eventually go full Dvorak because I like not having to move my fingers as much, but that's a step that I'll make when I finalize my own keyboard design.

## Numbers

A number bar makes no sense. I don't know why they're still a thing. Numpads are faster, easier, and more convenient.

## Layers

Chordal typing.
