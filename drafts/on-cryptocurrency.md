---
title: Thoughts on cryptocurrencies and the blockchain
date: 2022-11-22T21:13:16+01:00
taxonomies:
  tags: [thoughts]
---

I've had the fortune of not having cryptocurrency on my mind every
waking second of my life this past year. Unfortunately, this was not the
case for me for a few months in the year 2021.

fortunately, these days, cryptocurrency has pretty much died down. i
attribute its popularity in 2021 to the fact that pretty much nothing
was going on in the real world (other than the pandemic that had been
raging for basically a year) so the only thing people had to talk about
was the weird money thing that was happening on the internet. it had the
perfect mix of global reach, the american dream of getting rich quick
and making passive income,



i dont like the idea of "digital ownership". while the people who
are able to weave the chaos of the universe into beautiful art,
information, and ideas should absolutely be recognized and rewarded for
their accomplishments, i don't see "digital ownership" being a useful
concept moving forward. in the earliest recorded example, patents were
1 year long. speculation time: the future of art and information is
remixing. we need to adapt our information distribution and incentive
frameworks to account for this. as AI becomes more powerful, resources
and power and the ability to get things done is going to be more and
more consolidated. it is important that our social systems are updated
to keep up and ensure that we dont end up in a cyberpunk dystopia
(which, in the classic sense, is very unlikely, but i think a lot of
key ideas (megacorps, monopolization of technology) are still going to
be present.

markets are great for a lot of things, and are a brilliant tool for a
troublesome problem. however, i dont think using copyright as a shim to
force information and art to transform what is a potentially limitless
medium into the shackles of supply and demand is a productive move. that
being said, there arent a ton of better ideas out there. i think ko fi
and patreon are nice, and dont necessarily rely on copyright or any
other form of "digital ownership", but I think the amount of energy and
motivation going into finding creative solutions for alternative
monetization schemes are as there is going into doubling down on
ownership and finding ways to monetize the limitation of supply.

theres an artist i like whos doing cryptocurrency a bunch lately. they
just released an nft. i see this happening more in the future, both more
artists that i like as well as more nfts. that being said, i also see
this not lasting very long. when exploring the space, its important to
understand:
* where is the money coming from? seems like investment firms are
pouring a bunch into it because its basically a deregulated battlefield
where you can fuck people over without consequences
* why is the money flowing? what product or service are you providing?
* what is the value proposition? why is it good to have eth or an nft or
whatever?
* why does it need to be decentralized? hint: it doesnt. i love
decentralization as much as (or maybe more than) the next guy, but ive
seen in other places (namely p2p filesharing networks and social media
and chat) that its very costly, and is almost never worth it. the fact
that decentralization is just a fantasy with no substance is exemplified
(TODO: replace that word) by the fact that nft folks communicate pretty
much exclusively on twitter and discord instead of mastodon and matrix



"cryptocurrency does nothing to address 99% of the problems with
the banking industry. Because those problems are patterns of human
behavior. They're incentives, they're social structures, they're
modalities. The problem is what people are doing to others -- not that
the building they're doing it in has the word "BANK" written on the
side."

"They don't understand anything about the ecosystems they're trying to
disrupt. They only know that these are things that can be
conceptualized as valuable, and assume that because they understand one
very complicated thing (programming with cryptography), that all other
complicated things must be lesser in complexity, and naturally, lower
in the heirarchy of reality -- nails easily driven by the hammer that
they have created."

"In terms of improvements over Bitcoin, Ethereum has many. It's not
hard. Bitcoin sucks. In terms of problems with Bitcoin, Ethereum solves
none of them, and introduces a whole new suite of problems driven by
the techno-fetishistic egotism of assuming that programmers are
uniquely suited to solve society's problems."

42:00 "The end goal of this infinite machine is the financialization of
everything. Any benefits of digital uniqueness end up being a quirk, a
necessary precondition of turning everything into a stock market"

TODO: write a solutions doc later


line goes up chapter 10 is a pretty good explanation of the shittiness
of tokenomics and how marketing something as a financial asset attracts
a very strange crowd.


# technological flaws

* proof of work and blockchains and merkle trees already existed

# social flaws

* proof of stake just codifies the consolidation of capital
* theyre only value is that they're difficult (if not impossible) to
regulate. if regulation ever finds a way to hit them hard enough, they
will dissolve overnight, and if regulation finds a way to work
incrementally, they will sputter out (imo, this is more likely)
