Hexagonally tesselating pixel structure is a nearly uncompromisingly
better arrangement than the current popular choice, cartesian (square),
not only for geometric reasons, but also biological reasons. In this
essay, I will-

actually back my argument, rather than simply ending with my thesis
sentence for meme purposes. Geometrically, the most straightforward
argument for hextes pixels is that hexagons have a higher degree of
symmetry. Rather than the square's 4 axes, the hexagon has 6.

A common criticism of hextes patterns is that it can only represent
either vertical or horizontal lines perfectly, but not both. This
claim, however, is moot nowadays, as it only applies for lines a single
pixel wide, which are very rare in our modern digital interfaces.

Not only does the hextes better represent straight lines, but
for the same reasons, it is also much better for displaying curved
lines. Having more symmetry allows a line through the hextes to have
more flexibility in its representation.

In addition, hexagonal grids are more explicit in its definition
of adjacency, unlike square grids, in which cells can be either
side-adjacent or corner-adjacent. This is also part of the reason
that non-RTS strategy games like Catan and Civilization use hexagonal
maps.

Biologically, our eyes see using rods and cones. If you're not bored
yet, that probably means you already know how they work, but just in
case, the cones see color and the rods see movement/brightness (I'm
not a biologist, so this may be wrong). src: https://t.co/r3Uui2U4WU
https://t.co/ZnEDYyNDng

As you can see, the cones (the important ones in the middle of the eye)
are hexagonally arranged. By matching this arrangement, we can decrease
the minimum distance from eye-to-screen before individual pixels can
be seen.

While minimum eye-to-screen distance seems an inconsequential metric,
it becomes much more important when working with screens that must be
close to the eye. Now that VR, AR, and wearable technology on the rise,
this limitation in square screens will likely become a bottleneck
