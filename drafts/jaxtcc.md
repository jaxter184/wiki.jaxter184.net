---
title: jaxtcc
date: 2019-10-17T01:52:19-05:00
taxonomies:
  tags: [project, software, python]
---

# The problem

I think if I had to boil down all of my woes into a single most
influential issue in my life, it would be the fact that I have too many
things to do and too little time to do them. This is a simple problem
with two simple solutions: either find fewer things to do, or make more
time to do them. I have unsuccessfully attempted to solve this problem
many times throughout my life, and this project may be yet another
failed attempt. However, unlike my previous attempts, I have picked a
solution ambitious enough that it requires me to learn something new,
which will likely help me complete other tasks on my ever-growing list.

# Previous solutions

Over the past few years, I have begun to use my phone more
vigorously. Most prominently, I have developed various lists. I keep a
list of the activities that I do every day, a list of which days I've
fulfilled my German quota on Duolingo, how much I exercise, and more
on various note-taking apps. Of all the lists on my phone, my to-do
list is the one I refer to most. I've built a good habit of looking at
it to find out what I need to do. Unfortunately, I only ever look at
the top few entries of my list, where I have things I've promised to
do for other people, and tasks with deadlines. The vast majority of
my to-do list, which contains entries as simple and benign as "Learn
cloud names" and "Install Arch on desktop" in the same context as
more ambitious entries like "Earn a technical Grammy", goes completely
unnoticed in my day-to-day life. Through this project, I would like to
change that.

[, including Google Keep, Firefox Notes, Samsung Notes, and
Markor. Over the past few weeks, I've cut out Keep and Firefox Notes,
but unfortunately, until there is a viable way to take screen-off notes
on my Galaxy Note in Markor or some other free-source app that stores
my notes as files, Samsung Notes isn't going anywhere.]

# The proposed solution

## Work breakdown structure

> "The secret of getting ahead is getting started. The secret of
> getting started is breaking your complex, overwhelming tasks into
> small, manageable tasks, and starting on the first one."  
> \- Attributed to Mark Twain, but probably came before that

As much as I hate business-y buzzwords, I learned about work breakdown
structures in a communications class that was required for my
degree. They seem like a logical way to make the daunting seem more
doable, and tricking my brain into doing things it's scared of is
something I need to do more.

## Design

The biggest flaw of my to-do list is that there was no way to sort and
filter the list of tasks. Every task showed up in the given order,
regardless of what I felt like working on any given day. With this
list, even if I wanted to do something, it would require sifting
through over a hundred list entries to find out which ones were
relevant to the task I wanted, and even then, these entries probably
wouldn't give me the information I needed to work on my task. However,
my urges are rarely so specific. This results in me sorting through
lists with no idea what I want to do, only a primal urge telling me
that there is something in that list that will give me direction for
the next few hours, and that I need to find out what it is.

## The implementation

Python seemed like the right language for this. It's quick and easy,
and I find writing it to be therepeudic, which is nice for something
that I'm writing to procrastinate on working on actual things. Did I
say 'procrastinate on'? I meant 'assist with'. In terms of graphics
frameworks, I've used Tkinter in the past for this, but recently I've
really been a fan of GTK, so I figured I'd try that out. Another
recent interest I've had is YAML files. I really like that they're
human-readable, compact, and versatile. I've been creating YAML files
to a possibly unnecessary extent, and despite the fact that they're
easy to read, text editors unfortunately cannot sort and filter
entries. Essentially, this program is just going to be a YAML file
organizer.

# First steps

## GTK

A lot of fun. Highly recommend. The project was based around the
TreeView and ListView widgets. The first is used for a heirarchical
view of all my tasks and their breakdowns, and the ListView cuts
away the heirarchy so the tasks can be sorted agnostic of their
heirarchies. I had some issues figuring out exactly what I had to do
to update the views (spoiler: I was being dumb and it was actually a
lot easier than I thought it was) but overall, I'd say I had a positive
experience using it.

## Data structure

There was a lot of tweaking back and forth, but I ended up with a
pretty simple, clear structure of lists and key-value pairs. I ran into
a couple hiccups, mostly caused by the fact that I was trying to be
fancy and modular, but that ended up with me creating an unnecessary
amount of variables that all had the same thing in them.

## YAML vs CSV

CSV is a lot faster than YAML, and I really didn't need all the
nesting and such YAML provides. All of my arrays are converted to space
delimited strings at some point anyway.
