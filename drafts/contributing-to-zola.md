# background

I've made a few free/open source (FOSS) contributions over the last
few years. They've all been pretty simple contributions to fairly
low-profile repositories.

<!--
The first two I ever made were to embedded Rust projects
([avr-device][avr] and [ssd1306][ssd]), where I added support for
some piece of hardware that I wanted to use with the library. These
are some of the easiest contributions you can make, as they mostly
involve copying things over and extrapolating minor changes. I've also
contributed a few commits to [lv2-host-minimal][lv2], since I needed
them at the time to add LV2 support to [`tlature`](/tlature). This was
a relatively small project; I was the only contributor other than the
author when I made my first commit, which made it a little easier for
me to build up the motivation to contribute to it. <something about
how ive never considered contributing to blender because its such a big
project>
-->

This article outlines my process when making a (similarly simple)
contribution to `zola`. I am far from being an expert when it comes to
FOSS contributions, but I thought it would be a good idea to document
the process in order to demystify the act of contributing to a FOSS
project. Hopefully, in this case, my relative inexperience actually
makes this more useful than it would be if I were an expert in the
topic.

# motivation

if you've used FOSS before, and thought that you'd like to contribute
to it to show your appreciation, then my first recommendation is to
give up and donate money instead. Also, tell your friends about what
you like about it. It's just as beneficial, and its a lot easier to
contribute cash than it is to contribute time. However, if you're still
eager to help out after forking over some cash, then this retrospective
may help you figure out some best practices for making your first
contribution.

# git

This blog post assumes that you are vaguely familiar with the concept
of git, how it is different from GitHub, and what a pull request is. If
this is not the case for you, then the [Git Book][gbk] is a great
primer. Chapters 1, 2, and 3 should be more than enough background for the
things I talk about here.

# notifying the maintainer of your contribution

some people are rude, but thats their problem. most people are more
than happy that other people are invested enough in their project that
theyd be willing to volunteer their labor. With that in mind, while
it's totally understandable to be nervous about what a maintainer
will say to you, for the most part, there's not really anything to
worry about. Worst case, they'll say no, and you'll have wasted an
hour or two of your life. heck, that hour or two isnt even a complete
waste since you now have the experience you need to make your next
contribution even better. But chances are, youve solved a genuine
problem in the code, and the maintainer will be more than happy to work
with you to make sure your code feels right at home in the repository.

[ssd]: https://github.com/jamwaffles/ssd1306/commit/fc5a6f70e45a0ddb5b0e586f1ae8b84fcef583af
[avr]: https://github.com/Rahix/avr-device/pull/67
[lv2]: https://github.com/codybloemhard/lv2-host-minimal/pulls?q=is%3Apr+is%3Aclosed
[gbk]: https://git-scm.com/book/en/v2
