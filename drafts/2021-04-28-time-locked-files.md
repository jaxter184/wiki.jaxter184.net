---
layout: post
title:  "Time-locking Copyrighted Material"
date:   2020-04-28 15:01:19 -0500
permalink: time-lock
categories: jekyll thought
---

Copyright in the US is really weird. Not compared to other countries,
but just as a concept in general. If I'm correctly understanding my 30
seconds of Wikipedia research, copyright in general was created by the
church for censorship purposes. However, it's creation in the US was
to enrich the public domain by encouraging people to publicize their
ideas. It allows for a production monopoly of a given "intellectual
property," where the creator can sell licenses to publishers to allow
them to reproduce the work.

As much as I'd love to go into the benefits and drawbacks of such a
system, especially in the post-internet era, my focus today is on how a
remixer can publish their derivative work without explicit permission
from the copyright owner. Assuming the creation does not satisfy
the exceptions required for Fair Use, the only way to publish the
derivative work without a license is to wait until it is released into
the public domain. Of course, if the method I am proposing was as simple
as waiting 95 years, I wouldn't be writing a blog post about it.

# The proposition

In 2010, Julian Assange, as part of WikiLeaks, released an "insurance
file," which was an encrypted file that was distributed via BitTorrent.
The file itself now lies on many computers, in case something happens
to Assange. The "insurance" part relies on the fact that a few trusted
parties have access to the key, and will release it if something
happens to Assange. Of course, this has many flaws, one being that if
all the trusted parties are tracked and incapacitated (or they all lose
access to the key), the insurance file becomes practically nothing but
bytes of random noise.

We have a similar situation, where we have information that we wish to
lock away digitally, to be opened at some point in the future. However,
the biggest issue here is, again, our trusted party. Does there exist
a group of people on Earth who could be not only trusted to keep the
key a secret, but also be resistant enough to losing the key that they
could keep it for up to 95 years?

As with many other mundane, precise tasks, we have the option of
outsourcing it to a robot. My proposition is this: A steel box,
designed to be very difficult to break into, containing a plastic or
resin block, engraved with the 256 character key. The inside of the box
also contains a mechanism similar to a spring-wound watch, but much
slower, and much larger. When this watch reaches a certain position,
the steel box unlocks.

Essentially, a time-locked vault with an accompanying encrypted file. This is a boring idea and I should'nt really make a blog post about it.

# Potential problems

physical compromise

digital compromise

what if SHA256 becomes really easy

what happens when the creator of the original work dies

# Conclusion

Of course, this is mostly just idle thinking. For the most part,
a creator could just hold on to the derivative work privately until
the original work enters the public domain. The only cases where the
proposed scheme makes sense is if the creator is expected to die before
that time threshold. And copyrighted works lose much of their value
over time. For example, a Sherlock Holmes adaptation created when the
original books were being released may have some historical value, but
their entertainment value is much lower than modern adaptations like
those by Guy Ritchie and BBC. This value comes not just from the higher
production quality enabled by the improvement of technology over time,
but also because creative works are products of their time, and they
are more valuable in the times they are created due to the alignment
of the cultural understanding of the consumer and the cultural context
that the creator pulls their creation from, whether consciously or
otherwise.

