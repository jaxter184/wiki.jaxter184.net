### A quick anecdote from my childhood

When I was a kid, I had a little plastic blue box of trading cards.
It was a small collection, but every now and then, I would go through
the box and look at some of the cards.
The box contained cards from three different games: The Pokémon
Trading Card Game, Digimon (I don't think the card game had a distinct
name), and Magic the Gathering.
As of now, the only one that I have played from the three that were
burned into my childhood memory is Magic the Gathering.
It's a fun game, and a staple for any analogue game enthusiast,
especially the math-y logic-y type.
The culture surrounding rule interpretation is unique to this
game as far as I know, and to me, the rules and how people think about
them is the most interesting part of the game.
I don't consider myself a run-of-the-mill nerd, but I think that the
things that make games like this fun are appealing to a pretty wide
range of people.

(TODO: delete the above paragraph)

### [Enter Oko]

The second most interesting, and the topic of this blog post, is the
card (and its associated character) "Oko, Thief of Crowns".
I'm trying not to ramble too much about the game itself, but to make it
simple, Oko is one of a handful of important characters in the lore of
Magic the Gathering.
There are three reasons that it's my favorite card: It's colors are
green and blue, the way the card is played is pretty goofy, and the
artist is Korean.

### The project

#### Digits

Life, hit points, health.
There are many ways to describe the value that determines the important
metric that describes how far you are from dying in a game.
In MtG, this value starts at around 20 (depending on the particular
game format in question) and slowly descends at integer rates.
This allows me to limit my numerical display to 2 digits.
I could have used 7 segment displays, but thickness was an important
factor.
Other analogue tabletop games like Dungeons and Dragons have their
health metric at a similar scale (10-40ish), making 2 digits a
reasonable tradeoff.
Unfortunately, there are other games that use life metrics that are
much higher in magnitude, like Yu-Gi-Oh, where players start off with
8000 Life Points.


## The result

I think this is a case where showing the final project helps to explain
more than words can.

[!image]()

## The PCB

KiCAD has a couple tools that helped me create this device.
The big one was bitmap2component, which is a utility for converting
images to footprints.
While I'm glad the tool exists, there are a few things about it that
made it less than optimal to use.
The first is that there is no command-line utility.
Bitmap2component must be used from the GUI, which makes it unusable in
my makefile-based workflow.
Inkscape's command-line export is also suboptimal, but I was able to
finangle the file to make it work.
The other thing that I would like is a way to convert SVG files to
KiCAD components.
Right now, I export my SVGs at a resolution of 5953x8315, which is
a little bit of a waste, but I really want my shapes to come out
accurately.
I think I could reasonably export at 50%, or even 25% of that scale
without losing any detail at a visual level, but at this point, I don't
see any pressing reasons to make that trade.

## The buttons

I originally wanted to use Kailh Choc switches like I do in most of
my other projects, but I decided that having a low profile was more
important than anything else, so I decided on some rectangular tactile
switches.
Hopefully these have less bounce than the more common black square
tactile switches, but I'm not counting on it.
