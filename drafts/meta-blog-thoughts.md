* who is my audience?
	* people who want to learn
	* people who know me and care about what im up to
* what do they want to read?
	* how can they learn to do the things that i do
	* how is my life going? what have i been up to?
* how does this contribute to my own personal growth?
	* i have a better understanding of what i do
	* i can better organize the various thoughts that ive had
	* i can explain why i do the things that i do
* why do i need to justify the things i do?
	* so that others can do the same
	* so that others can not do the same
	* because its fun to defend my actions and behaviors
