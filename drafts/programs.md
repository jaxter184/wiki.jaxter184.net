---
layout: post
title:  "on software"
categories: jekyll thought software
---

The fact that software exists at all is amazing to me. The flexible, quick, cheap, and easy nature of software makes it an excellent playground for developing new ideas. While my personal interests are more in the realm of software as firmware for microcontrollers, this thought is mostly about software designed to run on general purpose computers.

I have been using computer software since I was in elementary school. I played all sorts of games, typed on word processors, edited images, and more. Over the years, I've found a small set of programs that I consider to be very well designed at a conceptual level. While the execution isn't perfect for some of these, I believe that the way they are designed makes them much more likely to eventually be almost universally used over their alternatives. This list of programs is:

* Blender
* FreeCAD
* Cockos Reaper
* vi/vim/neovim

The things that I like most about them is their:

* modularity
* command-line interfaces / scripting interface for automation
* customizability
* sustainability

For editing programs:

* parametric editing
* non-destructive workflows
