---
title: Terminology Reference
date: 2019-10-25
taxonomies:
  tags: [thoughts]
---

This page is a list of the terms that I like to use and why I prefer
them over their more commonly used counterparts.

### free-source
The Free Software Movement makes a good point that the term
'open-source' emphasizes a distinct connotation from the term
'free'. However, though the term 'free software' is intuitively
understood within this community, when speaking to an outsider or when
an outsider uses the term, there is ambiguity that must be clarified
every time, just in case there is confusion. On the other hand, the
term 'free-source' is not only distinct from 'open-source', but also
from 'free'. The speaker can avoid ambiguity by using the adjective
'free' to refer to the source code of the software in question rather
than the software itself, as source code almost never costs money, and
is more often either free or restricted, rather than free or paid. If
the outsider does not know anything about the Free Software movement,
the term 'free-source' raises a flag and prompts a question, while the
term 'free' is easily mistaken for "free of charge". An added bonus is
that FOSS can refer to (free/open)-source software, as in software that
is both free-source and open-source.

### pianjo
An alternative to keytar. Pretty self-explanatory, I think.
